# Process parameter address map files

parCols = {
	'Module':0,
	'Address':1,
	'Bytes':2,
	'Description':3,
	'Unit':4,
	'Display':5,
	'Bits':6,
	'Minimum':7,
	'Maximum':8,
	'Step':9
}

dpyCols = {
	'Name':0,
	'Kind':1,
	'Minimum':2,
	'Maximum':3,
	'Step':4,
	'Items':5
}

class Parameter:
	def __init__(self, md = "", adr = 0, byt = 1, nam = "", lbl = "", dpy = None, bit = 7, mn = 0, mx = 127, st = 1):
		self.module = md;
		self.address = adr
		self.bytes = byt
		self.name = nam
		self.label = lbl
		self.display = dpy
		self.bits = bit
		self.minimum = mn
		self.maximum = mx
		self.step = st

	def parse(self, l):
		'Parse list strings to parameter'
		if len(l) < 4:
			raise Exception('Invalid list size')
		try:
			self.module = l[0]
			self.address = int(l[1], 0)
			self.bytes = int(l[2], 0)
			self.name = l[3]
			self.label = l[4]
			self.display = l[5]
			self.bits = int(l[6], 0)
			self.minimum = int(l[7], 0)
			self.maximum = int(l[8], 0)
			self.step = int(l[9], 0)
		except IndexError:
			pass

	def code(self):
		'Format object into code'
		s = '\t\tp[i++] = new ParameterValueBits({0:#010x}, {1}, ps + "{2}", "{3}", 0, {4}, {5}, {6}, {7}, {8}, {9});'
		d = 'NULL'
		if len(self.display) and self.display != 'Integer': d = 'display[' + self.display + ']'
		s = s.format(self.address, self.bytes, self.name, self.label, d, self.bits, self.minimum, self.minimum, self.maximum, self.step)
		return s


class Display:
	def __init__(self, nam = "", knd = 'Integer', mn = 0, mx = 127, st = 1, itm = {}):
		self.name = nam;
		self.kind = knd;
		self.minimum = mn
		self.maximum = mx
		self.step = st
		self.items = itm

	def parse(self, l):
		"Parse list strings to parameter"
		if len(l) < 4:
			raise Exception('Invalid list size')
		try:
			self.name = l[0]
			if l[1] != 'Integer' and l[1] != 'Float':
				raise Exception('Invalid display kind')
			self.kind = l[1]
			if l[1] == 'Integer':
				self.minimum = int(l[2], 0)
				self.maximum = int(l[3], 0)
				self.step = int(l[4], 0)
			else:
				self.minimum = float(l[2])
				self.maximum = float(l[3])
				self.step = float(l[4])

			self.items = l[5]
		except IndexError:
			pass

	def code(self):
		'Format object into code'
		if len(self.items) == 0:
			t = '%d'
			if self.kind == 'Float': t = '%.1f'
			s = '\tdisplay[{}] = new ParameterDisplay{}({}, {}, {}, "{}");'
			s = s.format(self.name, self.kind, self.minimum, self.maximum, self.step, t)
		else:
			t = 'long'
			if self.kind == 'Float': t = 'float'
			s = '{{\n\tmap<{},string> m;\n'.format(t)
			dk = list(self.items.keys())
			dk.sort()
			for k in dk:
				s += '\tm[{}] = "{}";\n'.format(k, self.items[k])
			t = '\tdisplay[{}] = new ParameterDisplay{}({}, {}, {}, "{}", m);'
			t = t.format(self.name, self.kind, self.minimum, self.maximum, self.step, '%d')
			s += t + '\n}\n'
		return s

def parseItems(s):
	'Parse items in the form "value0:""string0"" value1:""string1"""'
	if s[0] == '"':
		s = s[1:-1]
	l = s.split()
	d = {}
	for i in l:
		j = i.split(':')
		if len(j) == 2:
			d[j[0]] = j[1][2:-2]
	return d



def parseParameterFile(fn):
	f = open(fn, 'rt')
	r = []
	for ln in f:
		l = ln.split(',') # Split CSV line
		l = list(map(lambda x: x.strip(), l)) # Strip whitespace
		r.append(l)
	f.close()
	return r[1:]

def parseDisplayFile(fn):
	f = open(fn, 'rt')
	r = []
	for ln in f:
		l = ln.split(',') # Split CSV line
		l = list(map(lambda x: x.strip(), l)) # Strip whitespace
		if len(l[-1]):
			l[-1] = parseItems(l[-1])
		r.append(l)
	f.close()
	return r[1:]

if __name__ == '__main__':
	import sys
	if len(sys.argv) < 3:
		print("Usage: {} par/dpy filename".format(sys.argv[0]))
	else:
		l = []
		if sys.argv[1] == 'par':
			r = parseParameterFile(sys.argv[2])
			for e in r:
				p = Parameter()
				p.parse(e)
				l.append(p)
		else:
			r = parseDisplayFile(sys.argv[2])
			for e in r:
				d = Display()
				d.parse(e)
				l.append(d)

		for e in l:
			print(e.code())
