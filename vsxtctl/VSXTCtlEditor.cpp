/** @file
	VSXTCtl GUI editor
*/

#include <Windows.h>
#include <WindowsX.h>
#include <tchar.h>

#include "VSXTCtl.h"

#ifdef WIN32
extern void *hInstance; // Set in VstPlugMain

static TCHAR szVSXTCtlClass[] = _T("VSXTCtlGUI");
static TCHAR szVSXTCtlTitle[] = _T("The VSXTCtl GUI");

static TCHAR szKnobClass[] = _T("VSXTCtlGUI_Knob");
static TCHAR szKnobTitle[] = _T("A VSXTCtl GUI Knob");

#define KNOB_PIXELS 32
#define KNOB_STEPS (992/32)
#define KNOB_RANGE 200

#endif

// Knob

int Knob::wcreg = 0;

Knob::Knob(HWND parent, int x, int y, AudioEffect *eff, VstInt32 idx, float dv)
: effect(eff), index(idx), defval(dv)
{
	if (!wcreg) // Register class
	{
		WNDCLASSEX wc;
		wc.cbSize = sizeof(WNDCLASSEX);
		wc.style = CS_HREDRAW | CS_VREDRAW; // CS_OWNDC
		wc.lpfnWndProc = WndProc;
		wc.cbClsExtra = 0;
		wc.cbWndExtra = 0;
		wc.hInstance = (HINSTANCE)hInstance;
		wc.hIcon = LoadIcon((HINSTANCE)hInstance, MAKEINTRESOURCE(IDI_APPLICATION));
		wc.hCursor = LoadCursor(NULL, IDC_ARROW);
		wc.hbrBackground = (HBRUSH)(COLOR_WINDOW + 1); // (HBRUSH)GetStockObject(WHITE_BRUSH)
		wc.lpszMenuName = NULL;
		wc.lpszClassName = szKnobClass;
		wc.hIconSm = LoadIcon((HINSTANCE)hInstance, MAKEINTRESOURCE(IDI_APPLICATION));

		if (!RegisterClassEx(&wc)) ;
	}
	++wcreg;

	bitmap = LoadBitmap((HINSTANCE)hInstance, MAKEINTRESOURCE(IDB_KNOB));
	
	hwnd = CreateWindowEx(
		0,
		szKnobClass, szKnobTitle, 
		WS_CHILD | WS_VISIBLE, // WS_OVERLAPPEDWINDOW
		x, y, // CW_USEDEFAULT, CW_USEDEFAULT,
		32, 32, 
		(HWND)parent, 
		NULL, // Menu or child id
		(HINSTANCE)hInstance,
		NULL // Window data pointer
	);
	SetWindowLong(hwnd, GWL_USERDATA, (long)this); // Attach object instance to window
}

Knob::~Knob()
{
	DestroyWindow(hwnd);
	--wcreg;
	if (!wcreg)
		UnregisterClass(szKnobClass, (HINSTANCE)hInstance);
}

LRESULT WINAPI
Knob::WndProc(HWND hWnd, UINT mesg, WPARAM wParam, LPARAM lParam)
{
	// static bool moving = false;
	static HWND prevw = NULL;
	static POINT cckpt;
	static float kval;
	BITMAP bm;
	Knob *knob = (Knob *)GetWindowLong(hWnd, GWL_USERDATA); // Get object pointer

	if (!knob)
		goto end;

	switch (mesg)
	{
	case WM_LBUTTONDOWN:
		cckpt.x = GET_X_LPARAM(lParam), cckpt.y = GET_Y_LPARAM(lParam);
		if (DragDetect(hWnd, cckpt)) 
		{
			// moving = true;
			// SetFocus(hWnd);
			prevw = SetCapture(hWnd); // Remember previous capture window
			kval = knob->getValue();
		}
		break;
	
	case WM_LBUTTONUP:
		SetCapture(prevw); // Restore previous capture window
		break;

	case WM_MOUSEMOVE:
		if (wParam & MK_LBUTTON)
		{
			int dy = cckpt.y- GET_Y_LPARAM(lParam);
			if (dy)
			{
				// float v = knob->getValue(), w = float(my)/KNOB_SCALE;
				float v = kval + float(dy)/KNOB_RANGE;
				v = (v < 0.f) ? 0.f : (v > 1.f) ? 1.f : v;
				knob->setValue(v);
				InvalidateRect(hWnd, NULL, FALSE);
			}
		}
		break;

	case WM_PAINT:
		{
			PAINTSTRUCT ps;
			int st = (int)(knob->getValue() * (KNOB_STEPS - 1));
			HDC hdc = BeginPaint(hWnd, &ps);

			HDC hmdc = CreateCompatibleDC(hdc);
			HBITMAP hbmo = (HBITMAP)SelectObject(hmdc, knob->getBitmap());
			GetObject(knob->getBitmap(), sizeof(BITMAP), &bm);
			BitBlt(hdc, 0, 0, bm.bmWidth, bm.bmWidth, hmdc, 0, st*bm.bmWidth, SRCCOPY);
			SelectObject(hdc, hbmo);
			DeleteDC(hmdc);

			EndPaint(hWnd, &ps);
			// knob->idle();
		}
		break;

	case WM_MOUSEWHEEL:
			{
				int ks = GET_KEYSTATE_WPARAM(wParam);
				int zd = GET_WHEEL_DELTA_WPARAM(wParam); // WHEEL_DELTA, which is 120
				float v = knob->getValue() + float(zd)*.0001f;
				v = (v < 0.f) ? 0.f : (v > 1.f) ? 1.f : v;
				knob->setValue(v);
				InvalidateRect(hWnd, NULL, FALSE);
			}
		break;

	case WM_KEYDOWN:
		{
			// k.character = wParam;
			// knob->onKeyDown(k);
			if (wParam == VK_UP || wParam == VK_DOWN)
			{
				float v = knob->getValue() + (wParam == VK_UP) ? 0.1f : -0.1f;
				v = (v < 0.f) ? 0.f : (v > 1.f) ? 1.f : v;
				knob->setValue(v);
				InvalidateRect(hWnd, NULL, FALSE);
			}
		}
		break;

	case WM_KEYUP:
		// k.character = wParam;
		// knob->onKeyUp(k);
		// InvalidateRect(hWnd, NULL, FALSE);
		break;

	case WM_DESTROY:
		PostQuitMessage(0);
		break;
	}

end:
	return DefWindowProc(hWnd, mesg, wParam, lParam);
}

// VSXTCtlGui

VSXTCtlGui::VSXTCtlGui(AudioEffect* effect)
: AEffEditor(effect)
{
	effect->setEditor(this);

	WNDCLASSEX wc;
	wc.cbSize = sizeof(WNDCLASSEX);
	wc.style = CS_HREDRAW | CS_VREDRAW; // CS_OWNDC
	wc.lpfnWndProc = WndProc;
	wc.cbClsExtra = 0;
	wc.cbWndExtra = 0;
	wc.hInstance = (HINSTANCE)hInstance;
	wc.hIcon = LoadIcon((HINSTANCE)hInstance, MAKEINTRESOURCE(IDI_APPLICATION));
	wc.hCursor = LoadCursor(NULL, IDC_ARROW);
	wc.hbrBackground = (HBRUSH)(COLOR_WINDOW + 1); // (HBRUSH)GetStockObject(WHITE_BRUSH)
	wc.lpszMenuName = NULL;
	wc.lpszClassName = szVSXTCtlClass;
	wc.hIconSm = LoadIcon((HINSTANCE)hInstance, MAKEINTRESOURCE(IDI_APPLICATION));

	if (!RegisterClassEx(&wc)) ;

	rect.top = 0;
	rect.left = 0;
	rect.bottom = 100;
	rect.right = 200;

	IStream *ps= NULL;
	
	// image = NULL;
}

VSXTCtlGui::~VSXTCtlGui()
{
	UnregisterClass(szVSXTCtlClass, (HINSTANCE)hInstance);
}

// Query editor size
bool
VSXTCtlGui::getRect(ERect **r)	
{ 
	*r = &rect; 
	return true;
}

bool 
VSXTCtlGui::open(void *ptr)
{
	AEffEditor::open(ptr);
	editorWindow = CreateWindowEx(
		0,
		szVSXTCtlClass, szVSXTCtlTitle, 
		WS_CHILD | WS_VISIBLE, // WS_OVERLAPPEDWINDOW
		0, 0, // CW_USEDEFAULT, CW_USEDEFAULT,
		rect.right - rect.left, 
		rect.bottom - rect.top,
		(HWND)systemWindow, 
		NULL, // Menu or child id
		(HINSTANCE)hInstance,
		NULL // Window data pointer
	);
	SetWindowLong(editorWindow, GWL_USERDATA, (long)this); // Attach object instance to window

	knob0 = new Knob(editorWindow, 100, 50, getEffect(), 0);
	
	image = LoadBitmap((HINSTANCE)hInstance, MAKEINTRESOURCE(IDB_DOG55));

	return true;
}

void 
VSXTCtlGui::close()				
{ 
	// ReleaseDC((HWND)editorWindow, dc);
	DeleteObject(image);
	delete knob0;

	DestroyWindow(editorWindow);
}				

void VSXTCtlGui::idle()
{
}

LRESULT WINAPI
VSXTCtlGui::WndProc(HWND hWnd, UINT mesg, WPARAM wParam, LPARAM lParam)
{
	VSXTCtlGui *ed = (VSXTCtlGui *)(GetWindowLong(hWnd, GWL_USERDATA));

	if (!ed)
		goto end;

	switch (mesg)
	{
	case WM_CREATE:
	// CreateStreamOnHGlobal(
	/*
	image = (HBITMAP)LoadImage((HINSTANCE)hInstance, _T("C:/Source/VSXTCtl/Resources/dog55.bmp"), 
		IMAGE_BITMAP, 0, 0, LR_CREATEDIBSECTION | LR_DEFAULTSIZE | LR_LOADFROMFILE);
	*/
		if (!ed->image)
			; // ed->image = LoadBitmap((HINSTANCE)hInstance, MAKEINTRESOURCE(IDB_DOG55));
		break;

	case WM_LBUTTONDOWN:
		SetFocus(hWnd);
		break;
	
	case WM_PAINT:
		{
			PAINTSTRUCT ps;
			TCHAR txt[] = _T("Test Sysex");

			HDC hdc = BeginPaint(hWnd, &ps);

			HGDIOBJ ho = SelectObject(hdc,GetStockObject(DC_PEN));
			// SelectObject(hdc, GetStockObject(BLACK_PEN));
			// SetDCBrushColor(hdc, RGB(255,0,0));
			SetDCPenColor(hdc, RGB(0,0,255));
			TextOut(hdc, 5, 5, txt, _tcslen(txt));
			SelectObject(hdc, ho);

			BITMAP bm;
			HDC hmdc = CreateCompatibleDC(hdc);
			HBITMAP hbmo = (HBITMAP)SelectObject(hmdc, ed->image);
			GetObject(ed->image, sizeof(BITMAP), &bm);
			BitBlt(hdc, 5, 30, bm.bmWidth, bm.bmHeight, hmdc, 0, 0, SRCCOPY);
			SelectObject(hdc, hbmo);
			DeleteDC(hmdc);

			EndPaint(hWnd, &ps);
			// ed->idle();
		}
		break;
	
	case WM_MOUSEWHEEL:
		{
			POINT p;
			GetCursorPos(&p);
			ScreenToClient(hWnd, &p);
			HWND cw = ChildWindowFromPoint(hWnd, p);
			if (cw && cw != hWnd)
				SendMessage(cw, WM_MOUSEWHEEL, wParam, lParam);
			else 
				ed->onWheel(float(GET_WHEEL_DELTA_WPARAM(wParam))/WHEEL_DELTA);
		}
		break;

	case WM_KEYDOWN:
		{
			POINT p;
			GetCursorPos(&p);
			ScreenToClient(hWnd, &p);
			HWND cw = ChildWindowFromPoint(hWnd, p);
			if (cw && cw != hWnd)
				SendMessage(cw, WM_KEYDOWN, wParam, lParam);
			else
			{
				VstKeyCode k;
				k.character = wParam;
				ed->onKeyDown(k);
			}
		}
		break;

	case WM_KEYUP:
		{
			VstKeyCode k;
			k.character = wParam;
			ed->onKeyUp(k);
		}
		break;

	case WM_DESTROY:
		// DeleteObject(ed->image);
		// ed->image = NULL;
		PostQuitMessage(0);
		break;
	}

end:
	return DefWindowProc(hWnd, mesg, wParam, lParam);
}

#if VST_2_1_EXTENSIONS

bool 
VSXTCtlGui::onKeyDown(VstKeyCode &keyCode)	
{ 
	return false; // Return true only if key was really used!

}	

bool 
VSXTCtlGui::onKeyUp(VstKeyCode &keyCode)		
{ 
	return false; 
}

bool 
VSXTCtlGui::onWheel(float distance)
{ 
	return false; 
}

// Set knob mode (if supported by Host). See CKnobMode in VSTGUI.
bool 
VSXTCtlGui::setKnobMode(VstInt32 val)			
{ 
	return false; 
}

#endif // VST_2_1_EXTENSIONS

