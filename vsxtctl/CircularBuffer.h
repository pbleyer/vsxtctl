/** @file
	Single-producer, single-consumer lock-free circular buffer support
	with unallocated slot.

	@author pbleyer
*/

#ifndef CIRCULAR_BUFFER_H_
#define CIRCULAR_BUFFER_H_

template <typename T, unsigned L>
class CircularBuffer
{
protected:
	unsigned _size;
	volatile unsigned _head;
	volatile unsigned _tail;
	T _buffer[L];

public:
	CircularBuffer()
	: _size(L), _head(0), _tail(0) { }

	/** Get the circular buffer size */
	inline unsigned size() { return _size; }

	/** Get the circular buffer current number of items
		@note Number of items may have changed after this call
	*/
	inline unsigned items()
	{
		volatile unsigned h = _head, t = _tail; // cache head, tail
		return (t < h) ? _size - (h - t) : t - h;
	}

	/** Get the circular buffer current available space
		@note Available space may have changed after this call
	*/
	inline unsigned available()
	{
		volatile unsigned h = _head, t = _tail; // cache head, tail
		return ((t < h) ? h - t : _size - (t - h)) - 1;
	}

	/** Clear a circular buffer */
	inline void clear() { _tail = _head; }

	/** Check if circular buffer is empty (head == tail) */
	inline bool empty() { return (_head == _tail); }

	/** Check if circular buffer is full ('next' tail is head) */
	inline bool full()
	{
		unsigned n = _tail + 1;
		if (n >= _size)
			n = 0; // wrap around
		return (n == _head);
	}

	/** Add single element to the buffer by value */
	inline unsigned put(const T &e)
	{
		unsigned n = _tail + 1; // next tail
		if (n >= _size)
			n = 0; // wrap around
		if (n == _head) // unavailable
			return 0;
		_buffer[_tail] = e;
		_tail = n;
		return 1;
	}

	/** Add elements to the buffer
		@return Number of elements successfully added to the buffer
	*/
	inline unsigned putArray(unsigned l, const T *e)
	{
		unsigned i = 0, n;
		while (i < l)
		{
			n = _tail + 1; // next tail
			if (n >= _size)
				n = 0; // wrap around
			if (n == _head)
				break; // unavailable
			_buffer[_tail] = e[i++];
			_tail = n;
		}
		return i; // number of elements added
	}

	/** Peek element from the buffer */
	inline unsigned peek(T *e)
	{
		if (_head == _tail)
			return 0; // empty
		*e = _buffer[_head];
		return 1; // done
	}

	/** Peek elements from the buffer */
	inline unsigned peekArray(unsigned l, T *e)
	{
		unsigned i = 0, h = _head;
		while (i < l)
		{
			if (h == _tail)
				break; // no more elements
			e[i++] = _buffer[h++];
			if (h >= _size)
				h = 0; // wrap around
		}
		return i;
	}

	/** Get or purge element from the buffer */
	inline unsigned get(T *e)
	{
		unsigned n = _head + 1; // next head
		if (_head == _tail)
			return 0; // empty
		if (n >= _size)
			n = 0; // wrap around
		if (e)
			*e = _buffer[_head];
		_head = n;
		return 1; // done
	}

	/** Get elements from the buffer */
	inline unsigned getArray(unsigned l, T *e)
	{
		unsigned i = 0, n;

		while (i < l)
		{
			if (_head == _tail)
				break; // empty
			n = _head + 1; // next head
			if (n >= _size)
				n = 0; // wrap around
			if (e)
				e[i] = _buffer[_head];
			++i;
			_head = n;
		}
		return i; // number of elements removed
	}
};

#endif // CIRCULAR_BUFFER_H_
