#ifndef MIDI_MESSAGE_H_
#define MIDI_MESSAGE_H_

#include <stdint.h>
#include <vector>

using std::vector;

class MidiMessage
{
public:
	enum Value
	{
		// Types
		None = 0, ///< Internal or none
		Off = 0x80, ///< Note off
		On = 0x90, ///< Note on
		Cc = 0xb0, ///< Continuous controller
		Pc = 0xc0, ///< Program change
		Aft = 0xa0, ///< Aftertouch
		Rpn = 0x64, ///< Registered parameter
		Nrpn = 0x62, ///< Non-registered parameter
		Syx = 0xf0, ///< Sysex

		// Constants
		SOX = 0xf0, // Sysex start
		EOX = 0xf7, // Sysex end
	};

public:
	uint8_t type; ///< Message type
	vector<uint8_t> data; ///< Message data

public:
	MidiMessage(uint8_t t = None, const vector<uint8_t> &d = vector<uint8_t>())
		: type(t), data(d) { }
	MidiMessage(const MidiMessage &m)
		: type(m.type), data(m.data) { }
	MidiMessage &operator=(const MidiMessage &m)
	{	type = m.type, data = m.data; }
	~MidiMessage() { }
};

#endif // MIDI_MESSAGE_H_