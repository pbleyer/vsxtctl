#include "ParameterDevice.h"
#include "Utility.h"

ParameterDevice::ParameterDevice(const ParameterDevice &d)
	: parameter(d.parameter.size())
{
	for (unsigned i = 0; i < parameter.size(); ++i)
		parameter[i] = new ParameterValue(*d.parameter[i]);
}

ParameterDevice::~ParameterDevice()
{
	vector<ParameterValue *>::const_iterator i = parameter.begin();
	while (i != parameter.end())
	{
		delete (*i);
		++i;
	}
}

bool
ParameterDevice::setParameter(int index, float value)
{
	ParameterValue *p = parameter[index];
	p->setParameter(value);
	return false;
}

int
ParameterDevice::getParameterIndex(uint32_t adr) const
{
	for (int i = 0; i < parameter.size(); ++i)
	{
		if (parameter[i]->address == adr)
			return i;
	}
	return -1;
}

ParameterGroup
ParameterDevice::getGroup(uint32_t adr, unsigned len) const
{
	ParameterGroup r;
	vector<ParameterValue *>::const_iterator i = parameter.begin();
	
	// Find parameter by address
	while (i != parameter.end())
	{
		if ((*i)->address == adr)
			break;
		++i;
	}

	if (i != parameter.end())
	{
		unsigned bs = 0;
		uint32_t a;

		// Push first one and initialize address
		if ((bs + (*i)->bytes) <= len)
		{
			r.parameter.push_back(*i);
			bs += (*i)->bytes;
			a = (*i)->address;
			++i;
		}

		while (i != parameter.end() && bs < len)
		{
			a = midiIncrement(4, a, (*i)->bytes);
			if (a != (*i)->address) // Non-contiguous
				break;

			bs += (*i)->bytes;
			if (bs > len) // Length exceeded
				break;

			r.parameter.push_back(*i);
			++i;
		}
	}

	return r;
}

list<ParameterGroup> 
ParameterDevice::getGroups(const vector<uint32_t> &adr, unsigned len) const
{
	list<ParameterGroup> r;

	vector<uint32_t>::const_iterator i = adr.begin();
	while (i != adr.end())
	{
		ParameterGroup g = getGroup(*i, len);
		if (g.parameter.size())
			r.push_back(g);
		++i;
	}
	return r;
}

vector<uint8_t> 
ParameterDevice::getParameterMap(unsigned idx, unsigned len) const
{
	vector<uint8_t> r;
	vector<ParameterValue *>::const_iterator i = parameter.begin() + idx;
	
	unsigned bs = 0;
	uint32_t a;

	// Push first one and initialize address
	if ((bs + (*i)->bytes) <= len)
	{
		r.push_back((*i)->formatMap);
		bs += (*i)->bytes;
		a = (*i)->address;
		++i;
	}

	while (i != parameter.end() && bs < len)
	{
		a = midiIncrement(4, a, (*i)->bytes);
		if (a != (*i)->address) // Non-contiguous
			break;

		bs += (*i)->bytes;
		if (bs > len) // Length exceeded
			break;

		r.push_back(*i);
		++i;
	}
}

void 
ParameterDevice::setParameterMap(unsigned idx, const vector<uint8_t> &v)
{

}