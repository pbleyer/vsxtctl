#ifndef ROLAND_VSYNTHXT_H_
#define ROLAND_VSYNTHXT_H_

#include "ParameterDevice.h"

class RolandVSynthXT: public ParameterDevice
{
public:
	enum Value
	{
		// Base addresses / sizes
		Setup_Base = 0x01000000,
		System_Base = 0x02000000,
		TemporaryPatch_Base = 0x10000000,
		TemporaryPatch_Size = 0x010000,
		TemporaryPatch_Items = 16,
		UserPatch_Base = 0x10000000,
		UserPatch_Size = 0x010000,
		UserPatch_Items = 512,

		SystemCommon_Offset = 0x000000,
		SystemController1_Offset = 0x004000,
		SystemController2_Offset = 0x005000,

		PatchCommon_Offset = 0x000000,
		PatchMFX_Offset = 0x000200,
		PatchChorus_Offset = 0x000400,
		PatchReverb_Offset = 0x000600,
		PatchController1_Offset = 0x000800,
		PatchController2_Offset = 0x000900,
		PatchStepModulator_Offset = 0x000A00,

		PatchOscillator_Offset = 0x001000,
		PatchOscillator_Size = 0x000100,
		PatchOscillator_Items = 16,

		PatchEnvelope_Offset = 0x002000,
		PatchEnvelope_Size = 0x000200,
		PatchEnvelope_Items = 16,

		PatchLFO_Offset = 0x004000,
		PatchLFO_Size = 0x000100,
		PatchLFO_Items = 16,

		PatchCOSM1_Offset = 0x005000,
		PatchCOSM2_Offset = 0x006000,
		PatchCOSM_Size = 0x000100,
		PatchCOSM_Items = 16,

		PatchArpeggio_Offset = 0x007000,
		PatchArpeggio_Size = 0x000100,
		PatchArpeggio_Items = 16,

		// Internal parameters
		ParameterRate = 0,
		ParameterPatchSelect,
		ParameterPadX,
		ParameterPadY,
		ParameterTimeTripX,
		ParameterTimeTripY,
		ParameterKnob1,
		ParameterKnob2,
		ParameterKnobE1,
		ParameterKnobE2,
		ParameterKnobE3,
		ParameterKnobE4,
		ParameterKnobE5,
		ParameterKnobE6,
		ParameterKnobE7,
		ParameterKnobE8,
		ParameterEND,

		// Named parameters
		ParameterPadXAssign = 124 + ParameterEND,
		ParameterPadYAssign,
		ParameterTimeTripXAssign,
		ParameterTimeTripYAssign,
		ParameterKnob1Assign,
		ParameterKnob2Assign,
		ParameterKnobE1Assign = 199 + ParameterEND,
		ParameterKnobE2Assign,
		ParameterKnobE3Assign,
		ParameterKnobE4Assign,
		ParameterKnobE5Assign,
		ParameterKnobE6Assign,
		ParameterKnobE7Assign,
		ParameterKnobE8Assign,

		// Display array index
		DisplayPatchSelect = 0,
		DisplayOffOn,
		Display5n_6p,
		Display3n_3p,
		Display100n0_100p0,
		Display24n_24p,
		DisplayMixParallel,
		DisplayInternalMidi,
		DisplayMidiChannel,
		Display50p_4000p,
		Display50p_20000p,
		Display2000p_20000p,
		Display15n_15p,
		Display0p5_8p0,
		DisplayKeyboardVelocity,
		DisplayKeyboardSensitivity,
		Display0_200p,
		DisplayPatchTransmit,
		DisplayCC01_CC95,
		DisplayStandardReverse,
		DisplayPedalAssign,
		DisplayASCII,
		Display48n_48p,
		Display50n_50p,
		DisplayMonoPoly,
		DisplayNormalLegato,
		DisplayRateTime,
		Display63n_63p,
		Display20p0_250p0,
		DisplayBypassOn,
		DisplayZone1_Zone16,
		DisplayC1n_G9p,
		Display100n_100p,
		DisplayMatrixControlSrc,
		DisplayMatrixControlDst,
		Display20000n_20000p,
		Display1p_3p,
		DisplayModType,
		Display200n_200p,
		DisplayL64_63R,
		DisplayOutputAssign,
		DisplayOscType,
		Display12n_12p,
		DisplayRandomPitchDepth,
		DisplayAnalogWaveform,
		Display0p0_4p0,
		DisplayWaveNumber,
		DisplayStartOffset,
		DisplayTimeOffset,
		DisplayPlaybackMode,
		DisplayOff1p_127p,
		DisplaySubOscOctave,
		DisplayTimeNotes,
		DisplayVelocityCurve,
		DisplayLFOWaveform,
		DisplayLFOOffset,
		DisplayLFOFadeMode,
		DisplayNoteCtrl,
		DisplayStepVelocity,
		DisplayOff1p_4p,
		DisplayXYTimeTrip,
		DisplayArpeggioMotif,
		Display16th8th,
		DisplayReal1p_127p,
		DisplayNoteGrid,
		Display1p_32p,
		DisplayKnobSet,
		DisplayOffPartVoice,
		DisplayStepSmooth,
		Display1p_16p,
		DisplayFwd123Bwd123,
		DisplayTrackDst,
		DisplayEND
	};

public:
	ParameterDisplay *display[DisplayEND]; ///< Parameter displays
	uint32_t patchAddress; ///< Current selected patch address base

public:
	RolandVSynthXT();
	virtual ~RolandVSynthXT(void);

	/** Check if parameter index is external or internal */
	bool isExternal(int idx) const { return idx >= ParameterEND; }

	void createDisplays();

	vector<ParameterValue *> createInternal(const char* s = "Int");

	vector<ParameterValue *> createSetup(const char* s = "Setup", uint32_t adr = Setup_Base);
	vector<ParameterValue *> createSystem(const char* s = "System", uint32_t adr = System_Base);
	vector<ParameterValue *> createPatch(const char* s = "", uint32_t ofs = 0);

	vector<ParameterValue *> createSystemCommon(const char* s = "SysCom", uint32_t adr = System_Base + SystemCommon_Offset);
	vector<ParameterValue *> createSystemController1(const char* s = "SysCtl1", uint32_t adr = System_Base + SystemController1_Offset);
	vector<ParameterValue *> createSystemController2(const char* s = "SysCtl2", uint32_t adr = System_Base + SystemController2_Offset);

	vector<ParameterValue *> createPatchCommon(const char* s = "PchCom", uint32_t ofs = PatchCommon_Offset);
	vector<ParameterValue *> createPatchMFX(const char* s = "PchMFX", uint32_t ofs = PatchMFX_Offset);
	vector<ParameterValue *> createPatchChorus(const char* s = "PchCho", uint32_t ofs = PatchChorus_Offset);
	vector<ParameterValue *> createPatchReverb(const char* s = "PchRev", uint32_t ofs = PatchReverb_Offset);
	vector<ParameterValue *> createPatchController1(const char* s = "PchCtl1", uint32_t ofs = PatchController1_Offset);
	vector<ParameterValue *> createPatchController2(const char* s = "PchCtl2", uint32_t ofs = PatchController2_Offset);
	vector<ParameterValue *> createPatchStepModulator(const char* s = "PchSMod", uint32_t ofs = PatchStepModulator_Offset);

	vector<ParameterValue *> createPatchOscillator(int zn = 0, const char* s = "PchOsc", uint32_t ofs = PatchOscillator_Offset);
	vector<ParameterValue *> createPatchEnvelope(int zn = 0, const char* s = "PchEnv", uint32_t ofs = PatchEnvelope_Offset);
	vector<ParameterValue *> createPatchLFO(int zn = 0, const char* s = "PchLFO", uint32_t ofs = PatchLFO_Offset);
	vector<ParameterValue *> createPatchCOSM(int zn = 0, const char* s = "PchCOSM1", uint32_t ofs = PatchCOSM1_Offset);
	vector<ParameterValue *> createPatchArpeggio(int nn = 0, const char* s = "PchArp", uint32_t ofs = PatchArpeggio_Offset);

	bool setParameter(int index, float value);
};

#endif // ROLAND_VSYNTHXT_H_

