#ifndef PARAMETER_DEVICE_H_
#define PARAMETER_DEVICE_H_

#include "ParameterValue.h"
#include "ParameterGroup.h"

#include <vector>
#include <list>
#include <algorithm>

#include <Windows.h>

#define ParameterDevice_DEFAULTRATE_ms 10

using std::vector;
using std::list;

class ParameterDevice
{
public:
	vector<ParameterValue *> parameter; ///< ParameterValue pointers
	int64_t rateTicks; ///< Rate in ticks at which value changes can be sent

public:
	ParameterDevice(const vector<ParameterValue *> &v = vector<ParameterValue *>())
		: parameter(v) { setRate(); }
	ParameterDevice(const ParameterDevice &d);
	virtual ~ParameterDevice(void);

	void setRate(uint32_t ms = ParameterDevice_DEFAULTRATE_ms)
	{
#ifdef WIN32
		int64_t f;
		QueryPerformanceFrequency((LARGE_INTEGER *)&f);
		rateTicks = (f * ms) / 1000;
#endif
	}

	void sort()
	{ 
		std::sort(parameter.begin(), parameter.end(),
			[](const ParameterValue *a, const ParameterValue *b) { return a->address < b->address; }
		);
	}

	/** Set parameter 
		@return true if parameter message needs to be sent
	*/
	virtual bool setParameter(int index, float value);

	/** Get parameter index by address */
	int getParameterIndex(uint32_t adr) const;

	/** Get list of parameter group providing start address and maximum number of bytes 
		@pre parameter array must be sorted by address
	*/
	ParameterGroup getGroup(uint32_t adr, unsigned l) const;

	/** Get list of contiguous parameter groups providing start address and maximum bytes
		@pre parameter array must be sorted by address
	*/
	list<ParameterGroup> getGroups(const vector<uint32_t> &adr, unsigned l) const;

	/** Get contiguous map of parameter values 
		
	*/
	vector<uint8_t> getParameterMap(unsigned idx, unsigned len) const;
	/** Set contiguous map of parameter values */
	void setParameterMap(unsigned idx, const vector<uint8_t> &v);

	/** Get current value of system tick counter */
	static inline int64_t tickCounter()
	{
		int64_t r;
#ifdef WIN32
		QueryPerformanceCounter((LARGE_INTEGER *)&r);
#endif
		return r;
	}

	/** Check if timestamp has elapsed, and update it if true
		@param cc Current counter value
		@param[in,out] lc Previous counter value
	*/
	inline bool hasElapsed(int64_t cc, int64_t &lc) 
	{
		if ((cc - lc) >= rateTicks)
		{
			lc = cc;
			return true;
		}
		else
			return false;
	}
};

#endif // PARAMETER_DEVICE_H_
