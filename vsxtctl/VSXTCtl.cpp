/** @file
	VSXTCtl module
*/

#define AVGFILTER_K (0.05f)
#define AVGFILTER(n,o,k) ((n)*(k) + (1.0f-(k))*(o))

#include "VSXTCtl.h"

// extern void testCSV(void);

// Constructor procedure
VSXTCTL_API AudioEffect* 
createEffectInstance(audioMasterCallback audioMaster)
{
	// testCSV();
	return new VSXTCtl(audioMaster);
}

/*
static vector<ParameterValue *> 
createParameters()
{
	map<long,string> ls;
	ls[-10] = "minus ten";
	ls[10] = "plus ten";
	ls[0] = "zero";
	ParameterDisplayInteger di(-10, 10, 1, "%+d", ls);

	vector<ParameterValue *> r;
	r.push_back(new ParameterValueBits(0x01000000, 2, "Part 1 Bank Select MSB:LSB", "Bank", 0, NULL,
		7, 0, 0, (1<<14)-1));
	r.push_back(new ParameterValueBits(0x01000002, 1, "Part 1 Program Number", "PC", 0, new ParameterDisplayInteger(di),
		7, 0, 0, (1<<7)-1));
	r.push_back(new ParameterValueBits(0x01000003, 1, "Test boolean", "bool", 0, NULL, 
		7, 0, 0, 1));
	
	return r;
}

static void
destroyParameters(const vector<ParameterValue *> &v)
{
	vector<ParameterValue *>::const_iterator i = v.begin();
	while (i != v.end())
	{
		delete *i;
		++i;
	}
}
*/

VSXTCtl::VSXTCtl(audioMasterCallback am)
	: AudioEffectX(am, 1, 0x37/*device.parameter.size()*/), device(), factory() 
{
	setNumInputs(2);
	setNumOutputs(2);
	setUniqueID('VSXT');
	canProcessReplacing();
	isSynth();
	// setEditor(new VSXTCtlGui(this));
	// override numParams
	cEffect.numParams = device.parameter.size();

	vst_strncpy(programName, "Default", kVstMaxProgNameLen);
	// device.parameter = createParameters();
}

VSXTCtl::~VSXTCtl()
{
	// destroyParameters(device.parameter);
}

// Effect
bool 
VSXTCtl::getEffectName(char *name)
{
	vst_strncpy(name, "VSXTCtl", kVstMaxEffectNameLen);
	return true;
}

bool 
VSXTCtl::getVendorString(char *text)
{
	vst_strncpy(text, "Canis Horrendus", kVstMaxVendorStrLen);
	return true;
}

bool 
VSXTCtl::getProductString(char *text)
{
	vst_strncpy(text, "VSXTCtl 0.1", kVstMaxProductStrLen);
	return true;
}

VstInt32 
VSXTCtl::getVendorVersion()
{
	return 0x0010;
}

VstInt32
VSXTCtl::canDo(char *text)
{
	/*
		"sendVstEvents", plug-in will send Vst events to Host
		"sendVstMidiEvent", plug-in will send MIDI events to Host
		"receiveVstEvents", plug-in can receive MIDI events from Host
		"receiveVstMidiEvent", plug-in can receive MIDI events from Host
		"receiveVstTimeInfo", plug-in can receive Time info from Host
		"offline", plug-in supports offline functions (offlineNotify, offlinePrepare, offlineRun)
		"midiProgramNames", plug-in supports function getMidiProgramName ()
		"bypass", plug-in supports function setBypass () 
		return: 1='cando', 0='don't know' (default), -1='No'
	*/

	if (!text)
		return 0;

	if (!strncmp(text, "hasCockosExtensions", 32))
		return 0xbeef0000;

	if (!strncmp(text, "sendVstMidiEvent", 32))
		return 1;

	if (!strncmp(text, "receiveVstMidiEvent", 32))
		return 1;

	return -1;
}

VstIntPtr 
VSXTCtl::vendorSpecific(VstInt32 lArg, VstIntPtr lArg2, void* ptrArg, float floatArg)
{
	switch (lArg)
	{
	case effGetParamDisplay:
		if (ptrArg)
		{
			device.parameter[(unsigned)lArg2]->getParameterDisplay(16/*kVstMaxParamStrLen*/, (char *)ptrArg, floatArg);
			return 0xbeef;
		}
	}
	return 0;
}

// Program
void 
VSXTCtl::setProgramName(char *name)
{
	vst_strncpy(programName, name, kVstMaxProgNameLen);
}

void 
VSXTCtl::getProgramName(char *name)
{
	vst_strncpy(name, programName, kVstMaxProgNameLen);
}

// Parameters
void 
VSXTCtl::setParameter(VstInt32 index, float value)
{
	ParameterValue *p = device.parameter[index];
	vector<uint8_t> *d = nullptr;
	bool e = device.hasElapsed(device.tickCounter(), p->last); // Check if time has elapsed and update counter

	if (!device.isExternal(index)) // Internal parameter
	{
		bool r = false;
		if ((p->flags & ParameterValue::Always) || p->isReset() || (p->testParameter(value) && e))
			r = device.setParameter(index, value);

		if (r) // Parameter needs message to be sent
		{
			switch (p->type)
			{
			case ParameterValue::Syx:
				d = factory.format(RolandFactory::DT, *p);
				break;

			case ParameterValue::Cc:
				{
					ParameterValue *q = nullptr;
					if (index == RolandVSynthXT::ParameterPadX)
						q = device.parameter[RolandVSynthXT::ParameterPadXAssign];
					else if (index == RolandVSynthXT::ParameterPadY)
						q = device.parameter[RolandVSynthXT::ParameterPadYAssign];
					else if (index == RolandVSynthXT::ParameterTimeTripX)
						q = device.parameter[RolandVSynthXT::ParameterTimeTripXAssign];
					else if (index == RolandVSynthXT::ParameterTimeTripY)
						q = device.parameter[RolandVSynthXT::ParameterTimeTripYAssign];
					else if (index == RolandVSynthXT::ParameterKnob1)
						q = device.parameter[RolandVSynthXT::ParameterKnob1Assign];
					else if (index == RolandVSynthXT::ParameterKnob2)
						q = device.parameter[RolandVSynthXT::ParameterKnob2Assign];
					else if (RolandVSynthXT::ParameterKnobE1 <= index && index <=  RolandVSynthXT::ParameterKnobE8)
						q = device.parameter[RolandVSynthXT::ParameterKnobE1Assign + (index - RolandVSynthXT::ParameterKnobE1)];

					if (q)
					{
						d = new vector<uint8_t>(3);
						uint32_t base = dynamic_cast<ParameterValueBits *>(q)->value;
						p->setQuery(d->size(), d->data(), base);
					}
					break;
				}
			}
		}
	}
	else 
	{
		if (p->isReset() || (p->testParameter(value) && e)) /** @todo p->reset() on transport play */
		{
			p->setParameter(value);
			d = factory.format(RolandFactory::DT, *p,
				(p->flags & ParameterValue::Patch) ? device.patchAddress : 0);
		}
	}

	if (d && !outBuffer.put(d)) 
		delete d; // Could not queue message, delete
}

float 
VSXTCtl::getParameter(VstInt32 index)
{
	return device.parameter[index]->getParameter();
}

void 
VSXTCtl::getParameterLabel(VstInt32 index, char *label)
{
	vst_strncpy(label, device.parameter[index]->label.c_str(), kVstMaxParamStrLen);
}

void 
VSXTCtl::getParameterDisplay(VstInt32 index, char *text)
{
	// dB2string(parameter0, text, kVstMaxParamStrLen);
	// vst_strncpy(text, device.parameter[index]->toString(), kVstMaxParamStrLen);
	// int l = _snprintf(text, 64, "%x ", device.parameter[index]->address);
	device.parameter[index]->getParameterDisplay(16/*kVstMaxParamStrLen*/, text);
}

void 
VSXTCtl::getParameterName(VstInt32 index, char *text)
{
	vst_strncpy(text, device.parameter[index]->name.c_str(), /* kVstMaxParamStrLen */ 64);
}

// Processing
void 
VSXTCtl::processReplacing(float **inputs, float **outputs, VstInt32 samples)
{
	// parameter0 = AVGFILTER(parameter0, parameter0_pre, AVGFILTER_K);
	// parameter0_pre = parameter0;

	int l = outBuffer.items();

	if (l) // Messages need to be sent
	{
		vector<uint8_t> **v = new vector<uint8_t>*[l];
		l = outBuffer.getArray(l, v);

		struct VstEvents *evs = (struct VstEvents *)
			malloc(sizeof(struct VstEvents) + ((l < 2) ? 0 : (l - 2)) * sizeof(VstEvent*));
		evs->numEvents = 0;
		evs->reserved = 0;
		VstInt32 df = samples/l;
		VstInt32 nf = 0;

		for (int i = 0; i < l; ++i)
		{
			struct VstEvent *ev = nullptr;
			uint8_t b = (*v[i])[0];

			if (b == ParameterValue::Syx)				
			{
				struct VstMidiSysexEvent *sev = (struct VstMidiSysexEvent *)
					malloc(sizeof(struct VstMidiSysexEvent));
				memset(sev, 0, sizeof(struct VstMidiSysexEvent));
				sev->type = kVstSysExType;
				sev->byteSize = sizeof(VstMidiSysexEvent);
				sev->deltaFrames = nf;
				// sev.flags = 0; 
				sev->dumpBytes = v[i]->size();
				sev->sysexDump = (char *)v[i]->data();
				ev = (VstEvent *)sev;
			}
			else if ((b & 0xf0) == ParameterValue::Cc)
			{
				struct VstMidiEvent *mev = (struct VstMidiEvent *)
					malloc(sizeof(struct VstMidiEvent));
				memset(mev, 0, sizeof(struct VstMidiEvent));
				mev->type = kVstMidiType;
				mev->byteSize = sizeof(VstMidiSysexEvent);
				mev->deltaFrames = nf;
				// sev.flags = 0; 
				memcpy(mev->midiData, v[i]->data(), 3);
				ev = (VstEvent *)mev;
			}

			nf += df;
			if (ev)
				evs->events[evs->numEvents++] = ev;
		}

		sendVstEventsToHost(evs);

		// Delete local events
		for (int i = l - 1; i >= 0; --i)
		{
			free(evs->events[i]);
			delete v[i];
		}
		free(evs);
		delete[] v;
	}

	// Process audio
	float* in1  =  inputs[0];
	float* in2  =  inputs[1];
	float* out1 = outputs[0];
	float* out2 = outputs[1];

	while (--samples >= 0)
	{
		(*out1++) = (*in1++);
		(*out2++) = (*in2++);
	}
}

VstInt32 
VSXTCtl::processEvents(VstEvents* events)
{
/*
	for (int j = 0; j < events->numEvents; ++j)
	{
		struct VstEvent *ev = events->events[j];
		struct VstEvent *ne = (struct VstEvent *)malloc(ev->byteSize);
		memcpy(ne, ev, ev->byteSize);
		evs->events[j] = ne;
	}
*/
	// Relay events to host
	sendVstEventsToHost(events);
	return 0;
}






/*
	// struct VstEvent ev;
	struct VstMidiEvent ev;
	memset(&ev, 0, sizeof(ev));
	ev.type = kVstMidiType;
	ev.byteSize = sizeof(VstMidiEvent);
	// ev.deltaFrames = 0;
	// ev.flags = 0; 
	// ev.noteLength = 0;
	// ev.noteOffset = 0;
	ev.midiData[0] = (tf) ? 0x90 : 0x80;
	ev.midiData[1] = 0x3c;
	ev.midiData[2] = 0x40;
	// ev.detune = 0;
	// ev.noteOffVelocity = 0;
	tf = !tf;

	RolandSysex rs(0x10, 0x0053, RolandSysex::RQ, 0x000000, 1);
	int rl = rs.length();
	uint8_t *sd = new uint8_t[2*rl];
	rs.format(rl, sd);
	rs.format(rl, sd+rl);

	struct VstMidiSysexEvent sev;
	memset(&sev, 0, sizeof(sev));
	sev.type = kVstSysExType;
	sev.byteSize = sizeof(VstMidiSysexEvent);
	// sev.deltaFrames = 0;
	// sev.flags = 0; 
	sev.dumpBytes = 2*rl;
	sev.sysexDump = (char *)sd;

	struct VstEvents evs;
	evs.numEvents = 1;
	evs.events[0] = (VstEvent *)&sev;
	evs.events[1] = (VstEvent *)&sev;

	sendVstEventsToHost(&evs);
	delete[] sd;
	*/
