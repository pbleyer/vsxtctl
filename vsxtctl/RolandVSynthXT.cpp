#include "RolandVSynthXT.h"

static char *MusicalPitch[] = { "C", "C#", "D", "D#", "E", "F", "F#", "G", "G#", "A", "A#", "B" };
static char *MusicalNotes[] = { "1/32", "1/16", "1/8�", "1/16�", "1/8", "1/4�",
	"1/8�", "1/4", "1/2�", "1/4�", "1/2", "1�", "1/2�", "1", "2�", "1�", "2"
};

RolandVSynthXT::RolandVSynthXT()
	: patchAddress(TemporaryPatch_Base)
{
	createDisplays();
	vector<ParameterValue *> v  = createInternal();
	parameter.insert(parameter.end(), v.begin(), v.end());
	v = createSetup();
	parameter.insert(parameter.end(), v.begin(), v.end());
	v = createSystem();
	parameter.insert(parameter.end(), v.begin(), v.end());
	v = createPatch();
	parameter.insert(parameter.end(), v.begin(), v.end());
}

RolandVSynthXT::~RolandVSynthXT()
{
	for (unsigned i = 0; i < DisplayEND; ++i)
		delete display[i];
}

bool
RolandVSynthXT::setParameter(int index, float value)
{
	ParameterValue *p = parameter[index];
	p->setParameter(value);

	switch (index)
	{
	case ParameterRate:
		{
			ParameterValueBits *q = dynamic_cast<ParameterValueBits *>(p);
			setRate((uint32_t)q->value);
			return false;
		}

	case ParameterPatchSelect:
		{
			ParameterValueBits *q = dynamic_cast<ParameterValueBits *>(p);
			patchAddress = (q->value < 16) ? TemporaryPatch_Base + q->value * TemporaryPatch_Size
				: UserPatch_Base + (q->value - 16) * UserPatch_Size;
			return false;
		}

	case ParameterPadX:
	case ParameterPadY:
	case ParameterTimeTripX:
	case ParameterTimeTripY:
	case ParameterKnob1:
	case ParameterKnob2:
	case ParameterKnobE1:
	case ParameterKnobE2:
	case ParameterKnobE3:
	case ParameterKnobE4:
	case ParameterKnobE5:
	case ParameterKnobE6:
	case ParameterKnobE7:
	case ParameterKnobE8:
		return true;
	}

	return false;
}

void
RolandVSynthXT::createDisplays()
{
	// display[Display0p_1000p] = new ParameterDisplayInteger("Display0p_1000p",0, 1000, 0, "%d");
	{
		map<long,string> m;
		char buf[32];
		unsigned i = 0;
		for (unsigned j = 0; j < 16; ++j)
		{
			_snprintf(buf, 32, "Temp (Part %d)", j+1);
			m[i++] = buf;
		}
		for (unsigned j = 0; j < 512; ++j)
		{
			_snprintf(buf, 32, "User (%03d)", j+1);
			m[i++] = buf;
		}
		display[DisplayPatchSelect] = new ParameterDisplayInteger("DisplayPatchSelect", 0, 16+512-1, 0, "%d", m);
	}

	{
		map<long,string> m;
		m[0] = "Off";
		m[1] = "On";
		display[DisplayOffOn] = new ParameterDisplayInteger("DisplayOffOn", 0, 1, 0, "%d", m);
	}
	display[Display5n_6p] = new ParameterDisplayInteger("Display5n_6p", -5, +6, 0, "%+d");
	display[Display3n_3p] = new ParameterDisplayInteger("Display3n_3p", -3, +3, 0, "%+d");

	display[Display100n0_100p0] = new ParameterDisplayFloat("Display100n0_100p0", -100.f, +100.f, 0, "%.1f");
	display[Display24n_24p] = new ParameterDisplayInteger("Display24n_24p", -24, +24, 0, "%+d");
	{
		map<long,string> m;
		m[0] = "Mix";
		m[1] = "Parallel";
		display[DisplayMixParallel] = new ParameterDisplayInteger("DisplayMixParallel", 0, 1, 0, "%d", m);
	}
	{
		map<long,string> m;
		m[0] = "Internal";
		m[1] = "MIDI";
		display[DisplayInternalMidi] = new ParameterDisplayInteger("DisplayInternalMidi", 0, 1, 0, "%d", m);
	}
	{
		map<long,string> m;
		char buf[8];
		for (unsigned j = 0; j < 16; ++j)
		{
			_snprintf(buf, 8, "Ch%02d", j+1);
			m[j] = buf;
		}
		display[DisplayMidiChannel] = new ParameterDisplayInteger("DisplayMidiChannel", 0, 15, 0, "%d", m);
	}

	display[Display50p_4000p] = new ParameterDisplayInteger("Display50p_4000p", 50, 4000, 0, "%d");
	display[Display50p_20000p] = new ParameterDisplayInteger("Display50p_20000p", 50, 20000, 0, "%d");
	display[Display2000p_20000p] = new ParameterDisplayInteger("Display2000p_20000p", 2000, 20000, 0, "%d");
	display[Display15n_15p] = new ParameterDisplayInteger("Display15n_15p", -15, 15, 0, "%+d");
	display[Display0p5_8p0] = new ParameterDisplayFloat("Display0p5_8p0", 0.5f, 8.0f, 0, "%.1f");

	{
		map<long,string> m;
		char buf[8];
		m[0] = "Off";
		for (unsigned j = 1; j < 96; ++j)
		{
			_snprintf(buf, 8, "CC%02d", j);
			m[j] = buf;
		}
		display[DisplayCC01_CC95] = new ParameterDisplayInteger("DisplayCC01_CC95", 0, 95, 0, "%d", m);
	}

	{
		map<long,string> m;
		m[0] = "Real";
		display[DisplayKeyboardVelocity] = new ParameterDisplayInteger("DisplayKeyboardVelocity", 0, 127, 0, "%d", m);
	}
	{
		map<long,string> m;
		m[0] = "Light";
		m[1] = "Medium";
		m[2] = "Heavy";
		display[DisplayKeyboardSensitivity] = new ParameterDisplayInteger("DisplayKeyboardSensitivity", 0, 2, 0, "%d", m);
	}

	display[Display0_200p] = new ParameterDisplayInteger("Display0_200p", 0, 200, 0, "%d");

	{
		map<long,string> m;
		char buf[8];
		for (unsigned j = 0; j < 16; ++j)
		{
			_snprintf(buf, 8, "Ch%02d", j+1);
			m[j] = buf;
		}
		m[16] = "Rx-Ch";
		m[17] = "Off";
		display[DisplayPatchTransmit] = new ParameterDisplayInteger("DisplayPatchTransmit", 0, 17, 0, "%d", m);
	}

	{
		map<long,string> m;
		m[0] = "Standard";
		m[1] = "Reverse";
		display[DisplayStandardReverse] = new ParameterDisplayInteger("DisplayStandardReverse", 0, 1, 0, "%d", m);
	}

	{
		map<long,string> m;
		char buf[8];
		m[0] = "Off";
		for (unsigned j = 1; j < 96; ++j)
		{
			_snprintf(buf, 8, "CC%02d", j);
			m[j] = buf;
		}
		m[96] = "Bend-up";
		m[97] = "Bend-down";
		m[98] = "Aft";
		display[DisplayPedalAssign] = new ParameterDisplayInteger("DisplayPedalAssign", 0, 98, 0, "%d", m);
	}

	display[DisplayASCII] = new ParameterDisplayInteger("DisplayASCII", 32, 127, 0, "%c");
	display[Display48n_48p] = new ParameterDisplayInteger("Display48n_48p", -48, 48, 0, "%+d");
	display[Display50n_50p] = new ParameterDisplayInteger("Display50n_50p", -50, 50, 0, "%+d");

	{
		map<long,string> m;
		m[0] = "Mono";
		m[1] = "Poly";
		display[DisplayMonoPoly] = new ParameterDisplayInteger("DisplayMonoPoly", 0, 1, 0, "%d", m);
	}
	{
		map<long,string> m;
		m[0] = "Normal";
		m[1] = "Legato";
		display[DisplayNormalLegato] = new ParameterDisplayInteger("DisplayNormalLegato", 0, 1, 0, "%d", m);
	}
	{
		map<long,string> m;
		m[0] = "Rate";
		m[1] = "Time";
		display[DisplayRateTime] = new ParameterDisplayInteger("DisplayRateTime", 0, 1, 0, "%d", m);
	}

	display[Display63n_63p] = new ParameterDisplayInteger("Display63n_63p", -63, 63, 0, "%+d");
	display[Display20p0_250p0] = new ParameterDisplayFloat("Display20p0_250p0", 20.f, 250.f, 0, "%.1f");

	{
		map<long,string> m;
		m[0] = "Bypass";
		m[1] = "On";
		display[DisplayBypassOn] = new ParameterDisplayInteger("DisplayBypassOn", 0, 1, 0, "%d", m);
	}

	{
		map<long,string> m;
		char buf[8];
		for (unsigned j = 0; j < 16; ++j)
		{
			_snprintf(buf, 8, "Zone%02d", j+1);
			m[j] = buf;
		}
		display[DisplayZone1_Zone16] = new ParameterDisplayInteger("DisplayZone1_Zone16", 0, 15, 0, "%d", m);
	}

	{
		map<long,string> m;
		char buf[8];
		for (unsigned j = 0; j < 128; ++j)
		{
			_snprintf(buf, 8, "%s%d", MusicalPitch[j%12], (j/12)-1);
			m[j] = buf;
		}
		display[DisplayC1n_G9p] = new ParameterDisplayInteger("DisplayC1n_G9p", 0, 127, 0, "%d", m);
	}

	display[Display100n_100p] = new ParameterDisplayInteger("Display100n_100p", -100, 100, 0, "%+d");

	{
		map<long,string> m;
		char buf[8];
		m[0] = "Off";
		for (unsigned j = 1; j < 96; ++j)
		{
			_snprintf(buf, 8, "CC%02d", j);
			m[j] = buf;
		}
		m[96] = "Bend";
		m[97] = "Aft";
		m[98] = "+Pad-X";
		m[99] = "+Pad-Y";
		m[100] = "Pad-X";
		m[101] = "Pad-Y";
		m[102] = "Trip-R";
		m[103] = "Beam-L";
		m[104] = "Beam-R";
		m[105] = "Knob1";
		m[106] = "Knob2";
		m[107] = "Velo";
		m[108] = "KeyF";
		display[DisplayMatrixControlSrc] = new ParameterDisplayInteger("DisplayMatrixControlSrc", 0, 108, 0, "%d", m);
	}
	{
		const char *mdn[] =
		{
			"Off",
			"Osc1-Pitch",
			"Osc1-Time/PW",
			"Osc1-Forma/Fat",
			"Osc1-Lvl",
			"Osc1-PEnv-Atk",
			"Osc1-PEnv-Dcy",
			"Osc1-PEnv-Rel",
			"Osc1-TEnv-Atk",
			"Osc1-TEnv-Dcy",
			"Osc1-TEnv-Rel",
			"Osc1-FEnv-Atk",
			"Osc1-FEnv-Dcy",
			"Osc1-FEnv-Rel",
			"Osc1-AEnv-Atk",
			"Osc1-AEnv-Dcy",
			"Osc1-AEnv-Rel",
			"Osc1-LFO-Rate",
			"Osc1-LFO-Pch",
			"Osc1-LFO-Tm/PW",
			"Osc1-LFO-Fr/Ft",
			"Osc1-LFO-Lvl",
			"Osc2-Pitch",
			"Osc2-Time/PW",
			"Osc2-Forma/Fat",
			"Osc2-Lvl",
			"Osc2-PEnv-Atk",
			"Osc2-PEnv-Dcy",
			"Osc2-PEnv-Rel",
			"Osc2-TEnv-Atk",
			"Osc2-TEnv-Dcy",
			"Osc2-TEnv-Rel",
			"Osc2-FEnv-Atk",
			"Osc2-FEnv-Dcy",
			"Osc2-FEnv-Rel",
			"Osc2-AEnv-Atk",
			"Osc2-AEnv-Dcy",
			"Osc2-AEnv-Rel",
			"Osc2-LFO-Rate",
			"Osc2-LFO-Pch",
			"Osc2-LFO-Tm/PW",
			"Osc2-LFO-Fr/Ft",
			"Osc2-LFO-Lvl",
			"CSM1-Prm1",
			"CSM1-Prm2",
			"CSM1-Env1-Atk",
			"CSM1-Env1-Dcy",
			"CSM1-Env1-Rel",
			"CSM1-Env2-Atk",
			"CSM1-Env2-Dcy",
			"CSM1-Env2-Rel",
			"CSM1-LFO-Rate",
			"CSM1-LFO-Prm1",
			"CSM1-LFO-Prm2",
			"CSM2-Prm1",
			"CSM2-Prm2",
			"CSM2-Env1-Atk",
			"CSM2-Env1-Dcy",
			"CSM2-Env1-Rel",
			"CSM2-Env2-Atk",
			"CSM2-Env2-Dcy",
			"CSM2-Env2-Rel",
			"CSM2-LFO-Rate",
			"CSM2-LFO-Prm1",
			"CSM2-LFO-Prm2",
			"TVA-Lvl",
			"TVA-Env-Atk",
			"TVA-Env-Dcy",
			"TVA-Env-Rel",
			"TVA-LFO-Rate",
			"TVA-LFO-Lvl",
			"TVA-LFO-Pan",
			"MFX-Send",
			"Cho-Send",
			"Rev-Send",
			"MFX-Prm1",
			"MFX-Prm2",
			"MFX-Prm3",
			"TVA-Pan"
		};
		map<long,string> m;
		for (unsigned j = 0; j < 79; ++j)
			m[j] = mdn[j];
		display[DisplayMatrixControlDst] = new ParameterDisplayInteger("DisplayMatrixControlDst", 0, 78, 0, "%d", m);
	}

	display[Display20000n_20000p] = new ParameterDisplayInteger("Display20000n_20000p", -20000, 20000, 0, "%+d");
	display[Display1p_3p] = new ParameterDisplayInteger("Display1p_3p", 1, 3, 0, "%d");
	{
		map<long,string> m;
		m[0] = "Mix";
		m[1] = "Ring(AM)";
		m[2] = "FM";
		m[3] = "Env Ring";
		m[4] = "Osc Sync";
		display[DisplayModType] = new ParameterDisplayInteger("DisplayModType", 0, 4, 0, "%d", m);
	}
	display[Display200n_200p] = new ParameterDisplayInteger("Display200n_200p", -200, 200, 0, "%+d");
	{
		map<long,string> m;
		char buf[8];
		for (unsigned j = 0; j < 64; ++j)
		{
			_snprintf(buf, 8, "L%02d", 64-j);
			m[j] = buf;
		}
		m[64] = "0";
		for (unsigned j = 65; j < 128; ++j)
		{
			_snprintf(buf, 8, "%02dR", j-64);
			m[j] = buf;
		}
		display[DisplayL64_63R] = new ParameterDisplayInteger("DisplayL64_63R", 0, 127, 0, "%d", m);
	}
	{
		map<long,string> m;
		m[0] = "Multi";
		m[1] = "Main";
		m[2] = "Dir";
		display[DisplayOutputAssign] = new ParameterDisplayInteger("DisplayOutputAssign", 0, 2, 0, "%d", m);
	}
	{
		map<long,string> m;
		m[0] = "Analog";
		m[1] = "PCM";
		m[2] = "ExtIn";
		display[DisplayOscType] = new ParameterDisplayInteger("DisplayOscType", 0, 2, 0, "%d", m);
	}

	display[Display12n_12p] = new ParameterDisplayInteger("Display12n_12p", -12, 12, 0, "%+d");

	{
		map<long,string> m;
		char buf[8];
		const unsigned val[] = { 0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 20, 30, 40,
			50, 60, 70, 80, 90, 100, 200, 300, 400, 500, 600, 700, 800, 900,
			1000, 1100, 1200
		};
		for (unsigned j = 0; j < 31; ++j)
		{
			_snprintf(buf, 8, "%d", val[j]);
			m[j] = buf;
		}
		display[DisplayRandomPitchDepth] = new ParameterDisplayInteger("DisplayRandomPitchDepth", 0, 30, 0, "%d", m);
	}
	{
		map<long,string> m;
		const char *val[] = { "Saw", "Square", "Triangle", "Sine", "Ramp", "Juno",
			"HQ-Saw", "HQ-Square", "Noise", "LA-Saw", "LA-Square", "Super-Saw",
			"Fback-Osc", "X-Mod-Osc"
		};
		for (unsigned j = 0; j < 14; ++j)
			m[j] = val[j];
		display[DisplayAnalogWaveform] = new ParameterDisplayInteger("DisplayAnalogWaveform", 0, 13, 0, "%d", m);
	}
	display[Display0p0_4p0] = new ParameterDisplayFloat("Display0p0_4p0", 0.f, 4.0f, 0, "%.1f");

	{
		map<long,string> m;
		m[0] = "Off";
		display[DisplayWaveNumber] = new ParameterDisplayInteger("DisplayWaveNumber", 0, 999, 0, "%d", m);
	}
	display[DisplayStartOffset] = new ParameterDisplayBigInteger("DisplayStartOffset", 0, 0xffffffff, 0, "%08x");
	{
		map<long,string> m;
		m[0] = "Bwd";
		m[1] = "Zero";
		m[2] = "Fwd";
		display[DisplayTimeOffset] = new ParameterDisplayInteger("DisplayTimeOffset", 0, 2, 0, "%d", m);
	}
	{
		map<long,string> m;
		m[0] = "Retrigger";
		m[1] = "Legato";
		m[2] = "Step";
		m[3] = "Event";
		display[DisplayPlaybackMode] = new ParameterDisplayInteger("DisplayPlaybackMode", 0, 3, 0, "%d", m);
	}
	{
		map<long,string> m;
		m[0] = "Off";
		display[DisplayOff1p_127p] = new ParameterDisplayInteger("DisplayOff1p_127p", 0, 127, 0, "%d", m);
	}
	{
		map<long,string> m;
		m[0] = "Off";
		m[1] = "-2";
		m[2] = "-1";
		m[3] = "0";
		display[DisplaySubOscOctave] = new ParameterDisplayInteger("DisplaySubOscOctave", 0, 3, 0, "%d", m);
	}
	{
		map<long,string> m;
		for (unsigned j = 0; j < 17; ++j)
			m[128+j] = MusicalNotes[j];
		display[DisplayTimeNotes] = new ParameterDisplayInteger("DisplayTimeNotes", 0, 144, 0, "%d", m);
	}
	{
		map<long,string> m;
		m[0] = "Fixed";
		display[DisplayVelocityCurve] = new ParameterDisplayInteger("DisplayVelocityCurve", 0, 7, 0, "%d", m);
	}

	{
		map<long,string> m;
		m[0] = "Sine";
		m[1] = "Triangle";
		m[2] = "Saw";
		m[3] = "Square";
		m[4] = "Random";
		m[5] = "Trapezoid";
		m[6] = "S&H";
		m[7] = "Chaos";
		display[DisplayLFOWaveform] = new ParameterDisplayInteger("DisplayLFOWaveform", 0, 7, 0, "%d", m);
	}
	display[DisplayLFOOffset] = new ParameterDisplayInteger("DisplayLFOOffset", -100, 100, 50, "%+d");
	{
		map<long,string> m;
		m[0] = "On-In";
		m[1] = "On-Out";
		m[2] = "Off-In";
		m[3] = "Off-Out";
		display[DisplayLFOFadeMode] = new ParameterDisplayInteger("DisplayLFOFadeMode", 0, 3, 0, "%d", m);
	}

	{
		map<long,string> m;
		m[0] = "Note";
		m[1] = "Ctrl";
		display[DisplayNoteCtrl] = new ParameterDisplayInteger("DisplayNoteCtrl", 0, 1, 0, "%d", m);
	}
	{
		map<long,string> m;
		m[0] = "Rest";
		m[128] = "Tie";
		display[DisplayStepVelocity] = new ParameterDisplayInteger("DisplayStepVelocity", 0, 128, 0, "%d", m);
	}

	{
		map<long,string> m;
		m[0] = "Off";
		display[DisplayOff1p_4p] = new ParameterDisplayInteger("DisplayOff1p_4p", 0, 4, 0, "%d", m);
	}
	{
		map<long,string> m;
		m[0] = "X-Y";
		m[1] = "Time-Trip";
		display[DisplayXYTimeTrip] = new ParameterDisplayInteger("DisplayXYTimeTrip", 0, 1, 0, "%d", m);
	}
	{
		map<long,string> m;
		m[0] = "Up";
		m[1] = "Down";
		m[2] = "Up&Down";
		m[3] = "Random";
		m[4] = "Note-Order";
		m[5] = "Rhythm";
		m[6] = "Phrase";
		m[7] = "Auto";
		display[DisplayArpeggioMotif] = new ParameterDisplayInteger("DisplayArpeggioMotif", 0, 7, 0, "%d", m);
	}
	{
		map<long,string> m;
		m[0] = "1/16";
		m[1] = "1/8";
		display[Display16th8th] = new ParameterDisplayInteger("Display16th8th", 0, 1, 0, "%d", m);
	}
	{
		map<long,string> m;
		m[0] = "Real";
		display[DisplayReal1p_127p] = new ParameterDisplayInteger("DisplayReal1p_127p", 0, 127, 0, "%d", m);
	}
	{
		map<long,string> m;
		m[0] = "1/4";
		m[1] = "1/8";
		m[2] = "1/8T";
		m[3] = "1/16";
		m[4] = "1/16T";
		m[5] = "1/32";
		display[DisplayNoteGrid] = new ParameterDisplayInteger("DisplayNoteGrid", 0, 5, 0, "%d", m);
	}
	display[Display1p_32p] = new ParameterDisplayInteger("Display1p_32p", 1, 32, 0, "%d");

	{
		map<long,string> m;
		char buf[8];
		for (unsigned j = 0; j < 8; ++j)
		{
			_snprintf(buf, 8, "Preset%d", j+1);
			m[j] = buf;
		}
		for (unsigned j = 0; j < 8; ++j)
		{
			_snprintf(buf, 8, "User%d", j+1);
			m[j+8] = buf;
		}
		display[DisplayKnobSet] = new ParameterDisplayInteger("DisplayKnobSet", 0, 15, 0, "%d", m);
	}

	{
		map<long,string> m;
		m[0] = "Off";
		m[1] = "Part";
		m[2] = "Voice";
		display[DisplayOffPartVoice] = new ParameterDisplayInteger("DisplayOffPartVoice", 0, 2, 0, "%d", m);
	}
	{
		map<long,string> m;
		m[0] = "Step";
		m[1] = "Smooth";
		display[DisplayStepSmooth] = new ParameterDisplayInteger("DisplayStepSmooth", 0, 1, 0, "%d", m);
	}
	display[Display1p_16p] = new ParameterDisplayInteger("Display1p_16p", 1, 16, 0, "%d");
	{
		map<long,string> m;
		char buf[8];
		for (unsigned j = 0; j < 3; ++j)
		{
			_snprintf(buf, 8, "Fwd%d", j+1);
			m[j] = buf;
		}
		for (unsigned j = 0; j < 3; ++j)
		{
			_snprintf(buf, 8, "Bwd%d", j+1);
			m[j+3] = buf;
		}
		display[DisplayFwd123Bwd123] = new ParameterDisplayInteger("DisplayFwd123Bwd123", 0, 5, 0, "%d", m);
	}
	{
		const char *tdn[] =
		{
			"Osc1+2-Pit-Crs",
			"Osc1+2-Pit-Fine",
			"Porta-Time",
			"Osc1-Pit-Crs",
			"Osc1-Pit-Fine",
			"Osc1-Pitch",
			"Osc1-PEnv-Dpt",
			"Osc1-Time/PW",
			"Osc1-TEnv-Dpt",
			"Osc1-Forma/Fat",
			"Osc1-FEnv-Dpt",
			"Osc1-LFO-Rate",
			"Osc1-LFO-Pch",
			"Osc1-LFO-Tm/PW",
			"Osc1-LFO-Fr/Ft",
			"Osc1-LFO-Lvl",
			"Osc1-Lvl",
			"Osc2-Pit-Crs",
			"Osc2-Pit-Fine",
			"Osc2-Pitch",
			"Osc2-PEnv-Dpt",
			"Osc2-Time/PW",
			"Osc2-TEnv-Dpt",
			"Osc2-Forma/Fat",
			"Osc2-FEnv-Dpt",
			"Osc2-LFO-Rate",
			"Osc2-LFO-Pch",
			"Osc2-LFO-Tm/PW",
			"Osc2-LFO-Fr/Ft",
			"Osc2-LFO-Lvl",
			"Osc2-Lvl",
			"CSM1-Prm1",
			"CSM1-Prm2",
			"CSM1-Env1-Atk",
			"CSM1-Env1-Dcy",
			"CSM1-Env1-Sus",
			"CSM1-Env1-Rel",
			"CSM1-Env2-Atk",
			"CSM1-Env2-Dcy",
			"CSM1-Env2-Sus",
			"CSM1-Env2-Rel",
			"CSM1-LFO-Rate",
			"CSM1-LFO-Prm1",
			"CSM1-LFO-Prm2",
			"CSM2-Prm1",
			"CSM2-Prm2",
			"CSM2-Env1-Atk",
			"CSM2-Env1-Dcy",
			"CSM2-Env1-Sus",
			"CSM2-Env1-Rel",
			"CSM2-Env2-Atk",
			"CSM2-Env2-Dcy",
			"CSM2-Env2-Sus",
			"CSM2-Env2-Rel",
			"CSM2-LFO-Rate",
			"CSM2-LFO-Prm1",
			"CSM2-LFO-Prm2",
			"TVA-Lvl",
			"TVA-Pan",
			"TVA-Env-Atk",
			"TVA-Env-Dcy",
			"TVA-Env-Sus",
			"TVA-Env-Rel",
			"TVA-LFO-Rate",
			"TVA-LFO-Lvl",
			"TVA-LFO-Pan",
			"MFX-Send",
			"Cho-Send",
			"Rev-Send",
			"MFX-Prm1",
			"MFX-Prm2",
			"MFX-Prm3"
		};
		map<long,string> m;
		for (unsigned j = 0; j < 72; ++j)
			m[j] = tdn[j];
		display[DisplayTrackDst] = new ParameterDisplayInteger("DisplayTrackDst", 0, 71, 0, "%d", m);
	}

}

vector<ParameterValue *>
RolandVSynthXT::createInternal(const char* s)
{
	string ps(s);
	ps += ": ";
	vector<ParameterValue *> p(ParameterEND);
	unsigned i = 0;
	// Create rate
	p[i++] = new ParameterValueBits(ParameterValue::None, -1, 2, ps + "Maximum Parameter Rate", "ms", ParameterValue::Always, NULL, 8, ParameterDevice_DEFAULTRATE_ms, 0, 1000);
	// Create patch select
	p[i++] = new ParameterValueBits(ParameterValue::None, -1, 2, ps + "Patch Select", "", ParameterValue::Always, display[DisplayPatchSelect], 8, 0, 0, 16+512-1);

	p[i++] = new ParameterValueBits(ParameterValue::Cc, -1, 1, ps + "Pad X", "", 0, NULL, 7, 0, 0, 127);
	p[i++] = new ParameterValueBits(ParameterValue::Cc, -1, 1, ps + "Pad Y", "", 0, NULL, 7, 0, 0, 127);
	p[i++] = new ParameterValueBits(ParameterValue::Cc, -1, 1, ps + "Time Trip X", "", 0, NULL, 7, 0, 0, 127);
	p[i++] = new ParameterValueBits(ParameterValue::Cc, -1, 1, ps + "Time Trip Y", "", 0, NULL, 7, 0, 0, 127);
	p[i++] = new ParameterValueBits(ParameterValue::Cc, -1, 1, ps + "Knob 1", "", 0, NULL, 7, 0, 0, 127);
	p[i++] = new ParameterValueBits(ParameterValue::Cc, -1, 1, ps + "Knob 2", "", 0, NULL, 7, 0, 0, 127);
	for (unsigned j = 0; j < 8; ++j)
	{
		char buf[16];
		_snprintf(buf, 16, "Knob E%d", j+1);
		p[i++] = new ParameterValueBits(ParameterValue::Cc, -1, 1, ps + buf, "", 0, NULL, 7, 0, 0, 127);
	}	

	return p;
}

vector<ParameterValue *>
RolandVSynthXT::createSetup(const char* s, uint32_t a)
{
	string ps(s);
	ps += ": ";
	vector<ParameterValue *> p(0x37);
	unsigned i = 0;
	// Create bank select and program change
	for (unsigned j = 0; j < 16; ++j)
	{
		char buf[64];
		_snprintf(buf, 64, "Part %d Bank Select MSB", j+1);
		p[i++] = new ParameterValueBits(MidiMessage::Syx, a++, 1, ps + buf, "MSB", 0, NULL, 7, 0, 0, 127);
		_snprintf(buf, 64, "Part %d Bank Select LSB", j+1);
		p[i++] = new ParameterValueBits(MidiMessage::Syx, a++, 1, ps + buf, "LSB", 0, NULL, 7, 0, 0, 127);
		_snprintf(buf, 64, "Part %d Program Number", j+1);
		p[i++] = new ParameterValueBits(MidiMessage::Syx, a++, 1, ps + buf, "PC", 0, NULL, 7, 0, 0, 127);
	}

	p[i++] = new ParameterValueBits(MidiMessage::Syx, a++, 1, ps + "Transpose Switch", "", 0, display[DisplayOffOn], 1, 0, 0, 1);
	p[i++] = new ParameterValueBits(MidiMessage::Syx, a++, 1, ps + "Transpose Value", "", 0, display[Display5n_6p], 7, 59+5, 59, 70);
	p[i++] = new ParameterValueBits(MidiMessage::Syx, a++, 1, ps + "Octave Shift", "", 0, display[Display3n_3p], 7, 61+3, 61, 67);
	p[i++] = new ParameterValueBits(MidiMessage::Syx, a++, 1, ps + "Beep Switch", "", 0, display[DisplayOffOn], 1, 0, 0, 1);
	p[i++] = new ParameterValueBits(MidiMessage::Syx, a++, 1, ps + "Sampling Template", "", 0, NULL, 4, 0, 1, 8);
	p[i++] = new ParameterValueBits(MidiMessage::Syx, a++, 1, ps + "Patch Palette Bank", "", 0, NULL, 3, 0, 0, 7);
	p[i++] = new ParameterValueBits(MidiMessage::Syx, a++, 1, ps + "Patch Palette Number", "", 0, NULL, 3, 0, 0, 7);
	return p;
}

vector<ParameterValue *>
RolandVSynthXT::createSystem(const char* s, uint32_t a)
{
	vector<ParameterValue *> r, v;
	v = createSystemCommon();
	r.insert(r.end(), v.begin(), v.end());
	v = createSystemController1();
	r.insert(r.end(), v.begin(), v.end());
	v = createSystemController2();
	r.insert(r.end(), v.begin(), v.end());
	return r;
}

vector<ParameterValue *>
RolandVSynthXT::createPatch(const char* s, uint32_t a)
{
	vector<ParameterValue *> r, v;
	v  = createPatchCommon();
	r.insert(r.end(), v.begin(), v.end());
	v  = createPatchMFX();
	r.insert(r.end(), v.begin(), v.end());
	v  = createPatchChorus();
	r.insert(r.end(), v.begin(), v.end());
	v  = createPatchReverb();
	r.insert(r.end(), v.begin(), v.end());
	v  = createPatchController1();
	r.insert(r.end(), v.begin(), v.end());
	v  = createPatchController2();
	r.insert(r.end(), v.begin(), v.end());
	v  = createPatchStepModulator();
	r.insert(r.end(), v.begin(), v.end());

	for (unsigned j = 0; j < 16; ++ j)
	{
		v  = createPatchOscillator(j);
		r.insert(r.end(), v.begin(), v.end());
	}
	for (unsigned j = 0; j < 16; ++ j)
	{
		v  = createPatchEnvelope(j);
		r.insert(r.end(), v.begin(), v.end());
	}
	for (unsigned j = 0; j < 16; ++ j)
	{
		v  = createPatchLFO(j);
		r.insert(r.end(), v.begin(), v.end());
	}
	for (unsigned j = 0; j < 16; ++ j)
	{
		v  = createPatchCOSM(j, "PchCOSM1", PatchCOSM1_Offset);
		r.insert(r.end(), v.begin(), v.end());
	}
	for (unsigned j = 0; j < 16; ++ j)
	{
		v  = createPatchCOSM(j, "PchCOSM2", PatchCOSM2_Offset);
		r.insert(r.end(), v.begin(), v.end());
	}
	for (unsigned j = 0; j < 16; ++ j)
	{
		v  = createPatchArpeggio(j);
		r.insert(r.end(), v.begin(), v.end());
	}

	// Set patch parameters relative flag
	vector<ParameterValue *>::const_iterator i = r.begin();
	while (i != r.end())
	{
		(*i)->flags |= ParameterValue::Patch;
		++i;
	}

	return r;
}

vector<ParameterValue *>
RolandVSynthXT::createSystemCommon(const char* s, uint32_t a)
{
	string ps(s);
	ps += ": ";
	vector<ParameterValue *> p(9+32+12);
	unsigned i = 0;

	p[i++] = new ParameterValueBits(MidiMessage::Syx, a, 4, ps + "Master Tune", "cents", 0, display[Display100n0_100p0], 4, 1024, 24, 2024);
	a += 4;
	p[i++] = new ParameterValueBits(MidiMessage::Syx, a++, 1, ps + "Master Key Shift", "", 0, display[Display24n_24p], 7, 64, 40, 88);
	p[i++] = new ParameterValueBits(MidiMessage::Syx, a++, 1, ps + "Master Level", "", 0, NULL, 7, 127, 0, 127);
	p[i++] = new ParameterValueBits(MidiMessage::Syx, a++, 1, ps + "Patch Remain", "", 0, display[DisplayOffOn], 1, 0, 0, 1);
	p[i++] = new ParameterValueBits(MidiMessage::Syx, a++, 1, ps + "Mix/Parallel", "", 0, display[DisplayMixParallel], 1, 0, 0, 1);
	p[i++] = new ParameterValueBits(MidiMessage::Syx, a++, 1, ps + "Clock Source", "", 0, display[DisplayInternalMidi], 1, 0, 0, 1);
	p[i++] = new ParameterValueBits(MidiMessage::Syx, a++, 1, ps + "Clock Out", "", 0, display[DisplayOffOn], 1, 0, 0, 1);
	p[i++] = new ParameterValueBits(MidiMessage::Syx, a++, 1, ps + "Receive Program Change", "", 0, display[DisplayOffOn], 1, 1, 0, 1);
	p[i++] = new ParameterValueBits(MidiMessage::Syx, a++, 1, ps + "Receive Bank Select", "", 0, display[DisplayOffOn], 1, 1, 0, 1);

	for (unsigned j = 0; j < 16; ++j)
	{
		char buf[64];
		_snprintf(buf, 64, "Part %d Receive Switch", j+1);
		p[i++] = new ParameterValueBits(MidiMessage::Syx, a++, 1, ps + buf, "", 0, display[DisplayOffOn], 1, 1, 0, 1);
		_snprintf(buf, 64, "Part %d Receive Channel", j+1);
		p[i++] = new ParameterValueBits(MidiMessage::Syx, a++, 1, ps + buf, "", 0, display[DisplayMidiChannel], 4, 0, 0, 15);
	}

	p[i++] = new ParameterValueBits(MidiMessage::Syx, a++, 1, ps + "EQ Low Freq", "Hz", 0, display[Display50p_4000p], 5, 0, 0, 19);
	p[i++] = new ParameterValueBits(MidiMessage::Syx, a++, 1, ps + "EQ Low Gain", "dB", 0, display[Display15n_15p], 5, 15, 0, 30);
	p[i++] = new ParameterValueBits(MidiMessage::Syx, a++, 1, ps + "EQ Mid 1 Freq", "Hz", 0, display[Display50p_20000p], 5, 0, 0, 26);
	p[i++] = new ParameterValueBits(MidiMessage::Syx, a++, 1, ps + "EQ Mid 1 Q", "", 0, display[Display0p5_8p0], 3, 0, 0, 5);
	p[i++] = new ParameterValueBits(MidiMessage::Syx, a++, 1, ps + "EQ Mid 1 Gain", "dB", 0, display[Display15n_15p], 5, 15, 0, 30);
	p[i++] = new ParameterValueBits(MidiMessage::Syx, a++, 1, ps + "EQ Mid 2 Freq", "Hz", 0, display[Display50p_20000p], 5, 0, 0, 26);
	p[i++] = new ParameterValueBits(MidiMessage::Syx, a++, 1, ps + "EQ Mid 2 Q", "", 0, display[Display0p5_8p0], 3, 0, 0, 5);
	p[i++] = new ParameterValueBits(MidiMessage::Syx, a++, 1, ps + "EQ Mid 2 Gain", "dB", 0, display[Display15n_15p], 5, 15, 0, 30);
	p[i++] = new ParameterValueBits(MidiMessage::Syx, a++, 1, ps + "EQ Hi Freq", "Hz", 0, display[Display2000p_20000p], 5, 0, 0, 8);
	p[i++] = new ParameterValueBits(MidiMessage::Syx, a++, 1, ps + "EQ Hi Gain", "dB", 0, display[Display15n_15p], 5, 15, 0, 30);
	p[i++] = new ParameterValueBits(MidiMessage::Syx, a++, 1, ps + "EQ Total Gain", "dB", 0, display[Display15n_15p], 5, 15, 0, 30);
	p[i++] = new ParameterValueBits(MidiMessage::Syx, a++, 1, ps + "EQ Switch", "", 0, display[DisplayOffOn], 1, 1, 0, 1);

	return p;
}

vector<ParameterValue *>
RolandVSynthXT::createSystemController1(const char* s, uint32_t a)
{
	string ps(s);
	ps += ": ";
	vector<ParameterValue *> p(27);
	unsigned i = 0;

	p[i++] = new ParameterValueBits(MidiMessage::Syx, a++, 1, ps + "Transmit Program Change", "", 0, display[DisplayOffOn], 1, 1, 0, 1);
	p[i++] = new ParameterValueBits(MidiMessage::Syx, a++, 1, ps + "Transmit Bank Select", "", 0, display[DisplayOffOn], 1, 1, 0, 1);
	p[i++] = new ParameterValueBits(MidiMessage::Syx, a++, 1, ps + "Keyboard Velocity", "", 0, display[DisplayKeyboardVelocity], 7, 0, 0, 127);
	p[i++] = new ParameterValueBits(MidiMessage::Syx, a++, 1, ps + "Keyboard Sensitivity", "", 0, display[DisplayKeyboardSensitivity], 2, 1, 0, 2);
	p[i++] = new ParameterValueBits(MidiMessage::Syx, a++, 1, ps + "Aftertouch Sensitivity", "", 0, display[Display0_200p], 5, 10, 0, 20);
	p[i++] = new ParameterValueBits(MidiMessage::Syx, a++, 1, ps + "Patch Transmit Channel", "", 0, display[DisplayPatchTransmit], 5, 0, 0, 17);

	p[i++] = new ParameterValueBits(MidiMessage::Syx, a++, 1, ps + "Beam Sensitivity L", "", 0, display[Display0_200p], 5, 10, 0, 20);
	p[i++] = new ParameterValueBits(MidiMessage::Syx, a++, 1, ps + "Beam Sensitivity R", "", 0, display[Display0_200p], 5, 10, 0, 20);
	p[i++] = new ParameterValueBits(MidiMessage::Syx, a++, 1, ps + "Beam 1 Assign L", "", 0, display[DisplayCC01_CC95], 7, 0, 0, 95);
	p[i++] = new ParameterValueBits(MidiMessage::Syx, a++, 1, ps + "Beam 1 Assign R", "", 0, display[DisplayCC01_CC95], 7, 0, 0, 95);
	p[i++] = new ParameterValueBits(MidiMessage::Syx, a++, 1, ps + "Beam 2 Assign L", "", 0, display[DisplayCC01_CC95], 7, 0, 0, 95);
	p[i++] = new ParameterValueBits(MidiMessage::Syx, a++, 1, ps + "Beam 2 Assign R", "", 0, display[DisplayCC01_CC95], 7, 0, 0, 95);
	p[i++] = new ParameterValueBits(MidiMessage::Syx, a++, 1, ps + "Beam 3 Assign L", "", 0, display[DisplayCC01_CC95], 7, 0, 0, 95);
	p[i++] = new ParameterValueBits(MidiMessage::Syx, a++, 1, ps + "Beam 3 Assign R", "", 0, display[DisplayCC01_CC95], 7, 0, 0, 95);
	p[i++] = new ParameterValueBits(MidiMessage::Syx, a++, 1, ps + "Beam 4 Assign L", "", 0, display[DisplayCC01_CC95], 7, 0, 0, 95);
	p[i++] = new ParameterValueBits(MidiMessage::Syx, a++, 1, ps + "Beam 4 Assign R", "", 0, display[DisplayCC01_CC95], 7, 0, 0, 95);

	p[i++] = new ParameterValueBits(MidiMessage::Syx, a++, 1, ps + "Pad Assign X", "", 0, display[DisplayCC01_CC95], 7, 80, 0, 95);
	p[i++] = new ParameterValueBits(MidiMessage::Syx, a++, 1, ps + "Pad Assign Y", "", 0, display[DisplayCC01_CC95], 7, 81, 0, 95);
	p[i++] = new ParameterValueBits(MidiMessage::Syx, a++, 1, ps + "Time Trip Assign X", "", 0, display[DisplayCC01_CC95], 7, 82, 0, 95);
	p[i++] = new ParameterValueBits(MidiMessage::Syx, a++, 1, ps + "Time Trip Assign Y", "", 0, display[DisplayCC01_CC95], 7, 83, 0, 95);

	p[i++] = new ParameterValueBits(MidiMessage::Syx, a++, 1, ps + "Knob 1 Assign", "", 0, display[DisplayCC01_CC95], 7, 0, 0, 95);
	p[i++] = new ParameterValueBits(MidiMessage::Syx, a++, 1, ps + "Knob 2 Assign", "", 0, display[DisplayCC01_CC95], 7, 0, 0, 95);

	p[i++] = new ParameterValueBits(MidiMessage::Syx, a++, 1, ps + "Hold Pedal Polarity", "", 0, display[DisplayStandardReverse], 1, 0, 0, 1);
	p[i++] = new ParameterValueBits(MidiMessage::Syx, a++, 1, ps + "Pedal 1 Assign", "", 0, display[DisplayPedalAssign], 7, 0, 0, 98);
	p[i++] = new ParameterValueBits(MidiMessage::Syx, a++, 1, ps + "Pedal 1 Polarity", "", 0, display[DisplayStandardReverse], 1, 0, 0, 1);
	p[i++] = new ParameterValueBits(MidiMessage::Syx, a++, 1, ps + "Pedal 2 Assign", "", 0, display[DisplayPedalAssign], 7, 0, 0, 98);
	p[i++] = new ParameterValueBits(MidiMessage::Syx, a++, 1, ps + "Pedal 2 Polarity", "", 0, display[DisplayStandardReverse], 1, 0, 0, 1);

	return p;
}

vector<ParameterValue *>
RolandVSynthXT::createSystemController2(const char* s, uint32_t a)
{
	string ps(s);
	ps += ": ";
	vector<ParameterValue *> p(0x48);
	unsigned i = 0;
	char buf[64];
	for (unsigned j = 0; j < 8; ++j)
		for (unsigned k = 0; k < 8; ++k)
		{
			_snprintf(buf, 64, "User Set %d Knob %d Parameter Assign", j+1, k+1);
			p[i++] = new ParameterValueBits(MidiMessage::Syx, a++, 1, ps + buf, "", 0, NULL, 5, 0, 0, 23);
		}

	const uint8_t knobcc[] = { 16, 17, 18, 19, 48, 49, 50, 51 }; // Default knob CCs
	for (unsigned j = 0; j < 8; ++j)
	{
		_snprintf(buf, 64, "Knob %d CC Assign", j+1);
		p[i++] = new ParameterValueBits(MidiMessage::Syx, a++, 1, ps + buf, "", 0, display[DisplayCC01_CC95], 7, knobcc[j], 0, 95);
	}

	return p;
}

vector<ParameterValue *>
RolandVSynthXT::createPatchCommon(const char* s, uint32_t a)
{
	string ps(s);
	ps += ": ";
	vector<ParameterValue *> p(12+18+16+1+12+8*5);
	unsigned i = 0;
	char buf[64];

	for (unsigned j = 0; j < 12; ++j)
	{
		_snprintf(buf, 64, "Patch Name %d", j+1);
		p[i++] = new ParameterValueBits(MidiMessage::Syx, a++, 1, ps + buf, "", 0, display[DisplayASCII], 7, 32, 32, 127);
	}
	p[i++] = new ParameterValueBits(MidiMessage::Syx, a++, 1, ps + "Patch Category", "", 0, NULL, 4, 0, 0, 15); /** @todo Add category display */

	p[i++] = new ParameterValueBits(MidiMessage::Syx, a++, 1, ps + "Patch Coarse Tune", "", 0, display[Display48n_48p], 7, 16+48, 16, 112);
	p[i++] = new ParameterValueBits(MidiMessage::Syx, a++, 1, ps + "Patch Fine Tune", "", 0, display[Display50n_50p], 7, 14+50, 14, 114);
	p[i++] = new ParameterValueBits(MidiMessage::Syx, a++, 1, ps + "Octave Shift", "", 0, display[Display3n_3p], 7, 61+3, 61, 67);
	p[i++] = new ParameterValueBits(MidiMessage::Syx, a++, 1, ps + "Mono/Poly", "", 0, display[DisplayMonoPoly], 1, 1, 0, 1);
	p[i++] = new ParameterValueBits(MidiMessage::Syx, a++, 1, ps + "Legato Switch", "", 0, display[DisplayOffOn], 1, 0, 0, 1);
	p[i++] = new ParameterValueBits(MidiMessage::Syx, a++, 1, ps + "Portamento Switch", "", 0, display[DisplayOffOn], 1, 0, 0, 1);
	p[i++] = new ParameterValueBits(MidiMessage::Syx, a++, 1, ps + "Portamento Mode", "", 0, display[DisplayNormalLegato], 1, 0, 0, 1);
	p[i++] = new ParameterValueBits(MidiMessage::Syx, a++, 1, ps + "Portamento Type", "", 0, display[DisplayRateTime], 1, 0, 0, 1);
	p[i++] = new ParameterValueBits(MidiMessage::Syx, a++, 1, ps + "Portamento Time", "", 0, NULL, 7, 0, 0, 127);
	p[i++] = new ParameterValueBits(MidiMessage::Syx, a++, 1, ps + "Portamento Time Velocity Sensitivity", "", 0, display[Display63n_63p], 7, 1+63, 1, 127);
	p[i++] = new ParameterValueBits(MidiMessage::Syx, a, 4, ps + "Patch Tempo", "", 0, display[Display20p0_250p0], 4, 1200, 200, 2500);
	a += 4;
	p[i++] = new ParameterValueBits(MidiMessage::Syx, a++, 1, ps + "Pitch Bend Range Up", "", 0, NULL, 6, 12, 0, 48);
	p[i++] = new ParameterValueBits(MidiMessage::Syx, a++, 1, ps + "Pitch Bend Range Down", "", 0, NULL, 6, 12, 0, 48);
	p[i++] = new ParameterValueBits(MidiMessage::Syx, a++, 1, ps + "MFX Switch", "", 0, display[DisplayBypassOn], 1, 0, 0, 1);
	p[i++] = new ParameterValueBits(MidiMessage::Syx, a++, 1, ps + "Chorus Switch", "", 0, display[DisplayOffOn], 1, 0, 0, 1);
	p[i++] = new ParameterValueBits(MidiMessage::Syx, a++, 1, ps + "Reverb Switch", "", 0, display[DisplayOffOn], 1, 0, 0, 1);

	p[i++] = new ParameterValueBits(MidiMessage::Syx, a++, 1, ps + "Current Zone", "", 0, display[DisplayZone1_Zone16], 4, 0, 0, 15);
	for (unsigned j = 0; j < 16; ++j)
	{
		_snprintf(buf, 64, "Zone %d Range Upper", j+1);
		p[i++] = new ParameterValueBits(MidiMessage::Syx, a++, 1, ps + buf, "", 0, display[DisplayC1n_G9p], 7, 0, 0, 127);
	}

	p[i++] = new ParameterValueBits(MidiMessage::Syx, a++, 1, ps + "Scale Tune Switch", "", 0, display[DisplayOffOn], 1, 0, 0, 1);
	for (unsigned j = 0; j < 12; ++j)
	{
		_snprintf(buf, 64, "Patch Scale Tune for %s", MusicalPitch[j]);
		p[i++] = new ParameterValueBits(MidiMessage::Syx, a, 2, ps + buf, "", 0, display[Display100n_100p], 4, 28+100, 28, 228);
		a += 2;
	}

	for (unsigned j = 0; j < 8; ++j)
	{
		_snprintf(buf, 64, "Matrix Control %d Source", j+1);
		p[i++] = new ParameterValueBits(MidiMessage::Syx, a++, 1, ps + buf, "", 0, display[DisplayMatrixControlSrc], 7, 0, 0, 108);
		_snprintf(buf, 64, "Matrix Control %d Dest 1", j+1);
		p[i++] = new ParameterValueBits(MidiMessage::Syx, a++, 1, ps + buf, "", 0, display[DisplayMatrixControlDst], 7, 0, 0, 78);
		_snprintf(buf, 64, "Matrix Control %d Sens 1", j+1);
		p[i++] = new ParameterValueBits(MidiMessage::Syx, a++, 1, ps + buf, "", 0, display[Display63n_63p], 7, 1+63, 1, 127);
		_snprintf(buf, 64, "Matrix Control %d Dest 2", j+1);
		p[i++] = new ParameterValueBits(MidiMessage::Syx, a++, 1, ps + buf, "", 0, display[DisplayMatrixControlDst], 7, 0, 0, 78);
		_snprintf(buf, 64, "Matrix Control %d Sens 2", j+1);
		p[i++] = new ParameterValueBits(MidiMessage::Syx, a++, 1, ps + buf, "", 0, display[Display63n_63p], 7, 1+63, 1, 127);
	}
	return p;
}

vector<ParameterValue *>
RolandVSynthXT::createPatchMFX(const char* s, uint32_t a)
{
	string ps(s);
	ps += ": ";
	vector<ParameterValue *> p(4+32);
	unsigned i = 0;
	char buf[64];

	p[i++] = new ParameterValueBits(MidiMessage::Syx, a++, 1, ps + "MFX Type", "", 0, NULL, 6, 0, 0, 41);
	p[i++] = new ParameterValueBits(MidiMessage::Syx, a++, 1, ps + "MFX Master Level", "", 0, NULL, 7, 0, 0, 127);
	p[i++] = new ParameterValueBits(MidiMessage::Syx, a++, 1, ps + "MFX Chorus Send Level", "", 0, NULL, 7, 0, 0, 127);
	p[i++] = new ParameterValueBits(MidiMessage::Syx, a++, 1, ps + "MFX Reverb Send Level", "", 0, NULL, 7, 0, 0, 127);

	for (unsigned j = 0; j < 32; ++j)
	{
		_snprintf(buf, 64, "MFX Parameter %d", j+1);
		p[i++] = new ParameterValueBits(MidiMessage::Syx, a, 4, ps + buf, "", 0, display[Display20000n_20000p], 7, 12768+20000, 12768, 52768);
		a += 4;
	}

	return p;
}

vector<ParameterValue *>
RolandVSynthXT::createPatchChorus(const char* s, uint32_t a)
{
	string ps(s);
	ps += ": ";
	vector<ParameterValue *> p(3+12);
	unsigned i = 0;
	char buf[64];

	p[i++] = new ParameterValueBits(MidiMessage::Syx, a++, 1, ps + "Chorus Type", "", 0, NULL, 4, 0, 0, 8);
	p[i++] = new ParameterValueBits(MidiMessage::Syx, a++, 1, ps + "Chorus Master Level", "", 0, NULL, 7, 0, 0, 127);
	p[i++] = new ParameterValueBits(MidiMessage::Syx, a++, 1, ps + "Chorus Reverb Send Level", "", 0, NULL, 7, 0, 0, 127);

	for (unsigned j = 0; j < 12; ++j)
	{
		_snprintf(buf, 64, "Chorus Parameter %d", j+1);
		p[i++] = new ParameterValueBits(MidiMessage::Syx, a, 4, ps + buf, "", 0, display[Display20000n_20000p], 7, 12768+20000, 12768, 52768);
		a += 4;
	}

	return p;
}

vector<ParameterValue *>
RolandVSynthXT::createPatchReverb(const char* s, uint32_t a)
{
	string ps(s);
	ps += ": ";
	vector<ParameterValue *> p(2+24);
	unsigned i = 0;
	char buf[64];

	p[i++] = new ParameterValueBits(MidiMessage::Syx, a++, 1, ps + "Reverb Type", "", 0, NULL, 4, 0, 0, 10);
	p[i++] = new ParameterValueBits(MidiMessage::Syx, a++, 1, ps + "Reverb Master Level", "", 0, NULL, 7, 0, 0, 127);

	for (unsigned j = 0; j < 24; ++j)
	{
		_snprintf(buf, 64, "Reverb Parameter %d", j+1);
		p[i++] = new ParameterValueBits(MidiMessage::Syx, a, 4, ps + buf, "", 0, display[Display20000n_20000p], 7, 12768+20000, 12768, 52768);
		a += 4;
	}

	return p;
}

vector<ParameterValue *>
RolandVSynthXT::createPatchStepModulator(const char* s, uint32_t a)
{
	string ps(s);
	ps += ": ";
	vector<ParameterValue *> p(2+4*(9+16));
	unsigned i = 0;
	char buf[64];

	p[i++] = new ParameterValueBits(MidiMessage::Syx, a++, 1, ps + "Master Switch", "", 0, display[DisplayOffOn], 1, 0, 0, 1);
	p[i++] = new ParameterValueBits(MidiMessage::Syx, a++, 1, ps + "Key Sync", "", 0, display[DisplayOffPartVoice], 2, 0, 0, 2);

	for (unsigned j = 0; j < 4; ++j)
	{
		_snprintf(buf, 64, "Track %d ", j+1);
		string pt = ps + buf;
		p[i++] = new ParameterValueBits(MidiMessage::Syx, a++, 1, pt + "Switch", "", 0, display[DisplayOffOn], 1, 0, 0, 1);
		p[i++] = new ParameterValueBits(MidiMessage::Syx, a++, 1, pt + "Loop Switch", "", 0, display[DisplayOffOn], 1, 0, 0, 1);
		p[i++] = new ParameterValueBits(MidiMessage::Syx, a++, 1, pt + "Type", "", 0, display[DisplayStepSmooth], 1, 0, 0, 1);
		p[i++] = new ParameterValueBits(MidiMessage::Syx, a++, 1, pt + "Grid", "", 0, display[DisplayNoteGrid], 3, 0, 0, 5);
		p[i++] = new ParameterValueBits(MidiMessage::Syx, a++, 1, pt + "End Point", "", 0, display[Display1p_16p], 4, 0, 0, 15);
		p[i++] = new ParameterValueBits(MidiMessage::Syx, a++, 1, pt + "Direction", "", 0, display[DisplayFwd123Bwd123], 3, 0, 0, 5);
		p[i++] = new ParameterValueBits(MidiMessage::Syx, a++, 1, pt + "Zone", "", 0, display[Display1p_16p], 4, 0, 0, 15);
		p[i++] = new ParameterValueBits(MidiMessage::Syx, a++, 1, pt + "Destination", "", 0, display[DisplayTrackDst], 7, 0, 0, 71);
		p[i++] = new ParameterValueBits(MidiMessage::Syx, a++, 1, pt + "Zone", "", 0, display[Display1p_16p], 4, 0, 0, 15);
		for (unsigned k = 0; k < 16; ++k)
		{
			_snprintf(buf, 64, "Step %02d Value", k+1);
			p[i++] = new ParameterValueBits(MidiMessage::Syx, a++, 1, pt + buf, "", 0, display[Display63n_63p], 7, 1+63, 1, 127);
		}
	}

	return p;
}

vector<ParameterValue *>
RolandVSynthXT::createPatchController1(const char* s, uint32_t a)
{
	string ps(s);
	ps += ": ";
	vector<ParameterValue *> p(0xd);
	unsigned i = 0;

	p[i++] = new ParameterValueBits(MidiMessage::Syx, a++, 1, ps + "Beam Type", "", 0, display[DisplayOff1p_4p], 3, 0, 0, 4);
	p[i++] = new ParameterValueBits(MidiMessage::Syx, a++, 1, ps + "Pad Mode", "", 0, display[DisplayXYTimeTrip], 1, 0, 0, 1);
	p[i++] = new ParameterValueBits(MidiMessage::Syx, a++, 1, ps + "Pad Hold", "", 0, display[DisplayOffOn], 1, 0, 0, 1);
	p[i++] = new ParameterValueBits(MidiMessage::Syx, a++, 1, ps + "Arpeggio Switch", "", 0, display[DisplayOffOn], 1, 0, 0, 1);
	p[i++] = new ParameterValueBits(MidiMessage::Syx, a++, 1, ps + "Arpeggio Hold", "", 0, display[DisplayOffOn], 1, 0, 0, 1);
	p[i++] = new ParameterValueBits(MidiMessage::Syx, a++, 1, ps + "Arpeggio Motif", "", 0, display[DisplayArpeggioMotif], 1, 0, 0, 1);
	p[i++] = new ParameterValueBits(MidiMessage::Syx, a++, 1, ps + "Arpeggio Duration", "", 0, NULL, 7, 0, 0, 100);
	p[i++] = new ParameterValueBits(MidiMessage::Syx, a++, 1, ps + "Arpeggio Shuffle Rate", "", 0, NULL, 7, 0, 0, 100);
	p[i++] = new ParameterValueBits(MidiMessage::Syx, a++, 1, ps + "Arpeggio Shuffle Resolution", "", 0, display[Display16th8th], 1, 0, 0, 1);
	p[i++] = new ParameterValueBits(MidiMessage::Syx, a++, 1, ps + "Arpeggio Keyboard Velocity", "", 0, display[DisplayReal1p_127p], 7, 0, 0, 127);
	p[i++] = new ParameterValueBits(MidiMessage::Syx, a++, 1, ps + "Arpeggio Octave Range", "", 0, display[Display3n_3p], 7, 61+3, 61, 67);
	p[i++] = new ParameterValueBits(MidiMessage::Syx, a++, 1, ps + "Arpeggio Grid", "", 0, display[DisplayNoteGrid], 3, 0, 0, 5);
	p[i++] = new ParameterValueBits(MidiMessage::Syx, a++, 1, ps + "Arpeggio End Point", "", 0, display[Display1p_32p], 5, 15, 0, 31);

	return p;
}

vector<ParameterValue *>
RolandVSynthXT::createPatchController2(const char* s, uint32_t a)
{
	string ps(s);
	ps += ": ";
	vector<ParameterValue *> p(16);
	unsigned i = 0;
	char buf[64];

	for (unsigned j = 0; j < 16; ++j)
	{
		_snprintf(buf, 64, "Zone %d Knob Set", j+1);
		p[i++] = new ParameterValueBits(MidiMessage::Syx, a++, 1, ps + buf, "", 0, display[DisplayKnobSet], 4, 0, 0, 15);
	}

	return p;
}

// Zoned parameters

vector<ParameterValue *>
RolandVSynthXT::createPatchOscillator(int zn, const char* s, uint32_t ofs)
{
	char buf[64];
	_snprintf(buf, 64, "%s Z[%d]: ", s, zn+1);
	string ps(buf);
	uint32_t a = ofs + zn * PatchOscillator_Size;
	vector<ParameterValue *> p(0x7f- 2*(3+7));
	unsigned i = 0;

	p[i++] = new ParameterValueBits(MidiMessage::Syx, a++, 1, ps + "Structure Type", "", 0, display[Display1p_3p], 2, 0, 0, 2);

	p[i++] = new ParameterValueBits(MidiMessage::Syx, a++, 1, ps + "Mod Switch", "", 0, display[DisplayOffOn], 1, 0, 0, 1);
	p[i++] = new ParameterValueBits(MidiMessage::Syx, a++, 1, ps + "Mod Type", "", 0, display[DisplayModType], 3, 0, 0, 4);
	p[i++] = new ParameterValueBits(MidiMessage::Syx, a++, 1, ps + "Mod Original Level", "", 0, NULL, 7, 0, 0, 127);
	p[i++] = new ParameterValueBits(MidiMessage::Syx, a++, 1, ps + "Mod Attack Time", "", 0, NULL, 7, 0, 0, 127);
	p[i++] = new ParameterValueBits(MidiMessage::Syx, a++, 1, ps + "Mod Release Time", "", 0, NULL, 7, 0, 0, 127);

	p[i++] = new ParameterValueBits(MidiMessage::Syx, a++, 1, ps + "TVA Switch", "", 0, display[DisplayOffOn], 1, 0, 0, 1);
	p[i++] = new ParameterValueBits(MidiMessage::Syx, a++, 1, ps + "TVA Level", "", 0, NULL, 7, 0, 0, 127);
	p[i++] = new ParameterValueBits(MidiMessage::Syx, a++, 1, ps + "TVA Level Keyfollow", "", 0, display[Display200n_200p], 7, 44+20, 44, 84);
	p[i++] = new ParameterValueBits(MidiMessage::Syx, a++, 1, ps + "TVA Level LFO Depth", "", 0, display[Display63n_63p], 7, 1+63, 1, 127);
	p[i++] = new ParameterValueBits(MidiMessage::Syx, a++, 1, ps + "TVA Pan", "", 0, display[DisplayL64_63R], 7, 64, 0, 127);
	p[i++] = new ParameterValueBits(MidiMessage::Syx, a++, 1, ps + "TVA Pan Keyfollow", "", 0, display[Display200n_200p], 7, 44+20, 44, 84);
	p[i++] = new ParameterValueBits(MidiMessage::Syx, a++, 1, ps + "TVA Pan LFO Depth", "", 0, display[Display63n_63p], 7, 1+63, 1, 127);

	p[i++] = new ParameterValueBits(MidiMessage::Syx, a++, 1, ps + "MFX Send Level", "", 0, NULL, 7, 0, 0, 127);
	p[i++] = new ParameterValueBits(MidiMessage::Syx, a++, 1, ps + "Chorus Send Level", "", 0, NULL, 7, 0, 0, 127);
	p[i++] = new ParameterValueBits(MidiMessage::Syx, a++, 1, ps + "Reverb Send Level", "", 0, NULL, 7, 0, 0, 127);
	p[i++] = new ParameterValueBits(MidiMessage::Syx, a++, 1, ps + "Output Assign", "", 0, display[DisplayOutputAssign], 7, 0, 0, 127);

	for (unsigned j = 0; j < 2; ++j)
	{
		_snprintf(buf, 64, "Osc%d ", j+1);
		string pt = ps + buf;
		p[i++] = new ParameterValueBits(MidiMessage::Syx, a++, 1, pt + "Switch", "", 0, display[DisplayOffOn], 1, 0, 0, 1);
		p[i++] = new ParameterValueBits(MidiMessage::Syx, a++, 1, pt + "Type", "", 0, display[DisplayOscType], 2, 0, 0, 2);
		p[i++] = new ParameterValueBits(MidiMessage::Syx, a++, 1, pt + "Wave Gain", "dB", 0, display[Display12n_12p], 7, 52+12, 52, 76);
		p[i++] = new ParameterValueBits(MidiMessage::Syx, a++, 1, pt + "Pitch", "", 0, display[Display63n_63p], 7, 1+63, 1, 127);
		p[i++] = new ParameterValueBits(MidiMessage::Syx, a++, 1, pt + "Pitch Keyfollow", "", 0, display[Display200n_200p], 7, 44+20, 44, 84);
		p[i++] = new ParameterValueBits(MidiMessage::Syx, a++, 1, pt + "Pitch LFO Depth", "", 0, display[Display63n_63p], 7, 1+63, 1, 127);
		p[i++] = new ParameterValueBits(MidiMessage::Syx, a++, 1, pt + "Pitch Envelope Depth", "", 0, display[Display63n_63p], 7, 1+63, 1, 127);
		p[i++] = new ParameterValueBits(MidiMessage::Syx, a++, 1, pt + "Coarse Tune", "", 0, display[Display48n_48p], 7, 16+48, 16, 112);
		p[i++] = new ParameterValueBits(MidiMessage::Syx, a++, 1, pt + "Fine Tune", "cent", 0, display[Display50n_50p], 7, 14+50, 14, 114);
		p[i++] = new ParameterValueBits(MidiMessage::Syx, a++, 1, pt + "Random Pitch Depth", "", 0, display[DisplayRandomPitchDepth], 5, 0, 0, 30);
		p[i++] = new ParameterValueBits(MidiMessage::Syx, a++, 1, pt + "Level", "", 0, NULL, 7, 0, 0, 127);
		p[i++] = new ParameterValueBits(MidiMessage::Syx, a++, 1, pt + "Level Keyfollow", "", 0, display[Display200n_200p], 7, 44+20, 44, 84);
		p[i++] = new ParameterValueBits(MidiMessage::Syx, a++, 1, pt + "Level LFO Depth", "", 0, display[Display63n_63p], 7, 1+63, 1, 127);

		p[i++] = new ParameterValueBits(MidiMessage::Syx, a++, 1, pt + "Analog Waveform", "", 0, display[DisplayAnalogWaveform], 4, 0, 0, 13);
		p[i++] = new ParameterValueBits(MidiMessage::Syx, a++, 1, pt + "Impact", "", 0, display[Display0p0_4p0], 6, 0, 0, 40);
		p[i++] = new ParameterValueBits(MidiMessage::Syx, a++, 1, pt + "Pulse Width", "", 0, display[Display63n_63p], 7, 1+63, 1, 127);
		p[i++] = new ParameterValueBits(MidiMessage::Syx, a++, 1, pt + "Pulse Width Keyfollow", "", 0, display[Display200n_200p], 7, 44+20, 44, 84);
		p[i++] = new ParameterValueBits(MidiMessage::Syx, a++, 1, pt + "Pulse Width LFO Depth", "", 0, display[Display63n_63p], 7, 1+63, 1, 127);
		p[i++] = new ParameterValueBits(MidiMessage::Syx, a++, 1, pt + "Pulse Width Envelope Depth", "", 0, display[Display63n_63p], 7, 1+63, 1, 127);
		p[i++] = new ParameterValueBits(MidiMessage::Syx, a++, 1, pt + "Fat", "", 0, NULL, 7, 0, 0, 127);
		p[i++] = new ParameterValueBits(MidiMessage::Syx, a++, 1, pt + "Fat Keyfollow", "", 0, display[Display200n_200p], 7, 44+20, 44, 84);
		p[i++] = new ParameterValueBits(MidiMessage::Syx, a++, 1, pt + "Fat LFO Depth", "", 0, display[Display63n_63p], 7, 1+63, 1, 127);
		p[i++] = new ParameterValueBits(MidiMessage::Syx, a++, 1, pt + "Fat Envelope Depth", "", 0, display[Display63n_63p], 7, 1+63, 1, 127);
		p[i++] = new ParameterValueBits(MidiMessage::Syx, a, 4, pt + "Wave Number", "", 0, display[DisplayWaveNumber], 4, 0, 0, 999);
		a += 4;
		p[i++] = new ParameterValueBits(MidiMessage::Syx, a, 8, pt + "Start Offset", "", 0, display[DisplayStartOffset], 4, 0, 0, 0xffffffff);
		a += 8;
		p[i++] = new ParameterValueBits(MidiMessage::Syx, a++, 1, pt + "Time", "", 0, display[Display63n_63p], 7, 1+63, 1, 127);
		p[i++] = new ParameterValueBits(MidiMessage::Syx, a++, 1, pt + "Time Keyfollow", "", 0, display[Display200n_200p], 7, 44+20, 44, 84);
		p[i++] = new ParameterValueBits(MidiMessage::Syx, a++, 1, pt + "Time LFO Depth", "", 0, display[Display63n_63p], 7, 1+63, 1, 127);
		p[i++] = new ParameterValueBits(MidiMessage::Syx, a++, 1, pt + "Time Envelope Depth", "", 0, display[Display63n_63p], 7, 1+63, 1, 127);
		p[i++] = new ParameterValueBits(MidiMessage::Syx, a++, 1, pt + "Time Offset", "", 0, display[DisplayTimeOffset], 2, 0, 0, 2);
		p[i++] = new ParameterValueBits(MidiMessage::Syx, a++, 1, pt + "Formant", "", 0, display[Display63n_63p], 7, 1+63, 1, 127);
		p[i++] = new ParameterValueBits(MidiMessage::Syx, a++, 1, pt + "Formant Keyfollow", "", 0, display[Display200n_200p], 7, 44+20, 44, 84);
		p[i++] = new ParameterValueBits(MidiMessage::Syx, a++, 1, pt + "Formant LFO Depth", "", 0, display[Display63n_63p], 7, 1+63, 1, 127);
		p[i++] = new ParameterValueBits(MidiMessage::Syx, a++, 1, pt + "Formant Envelope Depth", "", 0, display[Display63n_63p], 7, 1+63, 1, 127);
		p[i++] = new ParameterValueBits(MidiMessage::Syx, a++, 1, pt + "Tempo Sync Switch", "", 0, display[DisplayOffOn], 1, 0, 0, 1);
		p[i++] = new ParameterValueBits(MidiMessage::Syx, a++, 1, pt + "Robot Voice Switch", "", 0, display[DisplayOffOn], 1, 0, 0, 1);
		p[i++] = new ParameterValueBits(MidiMessage::Syx, a++, 1, pt + "Playback Mode", "", 0, display[DisplayPlaybackMode], 2, 0, 0, 3);
		p[i++] = new ParameterValueBits(MidiMessage::Syx, a++, 1, pt + "Loop Switch", "", 0, display[DisplayOffOn], 1, 0, 0, 1);
		p[i++] = new ParameterValueBits(MidiMessage::Syx, a++, 1, pt + "Vari Switch", "", 0, display[DisplayOffOn], 1, 0, 0, 1);
		p[i++] = new ParameterValueBits(MidiMessage::Syx, a++, 1, pt + "Energy", "", 0, display[DisplayOff1p_127p], 7, 0, 0, 127);
		p[i++] = new ParameterValueBits(MidiMessage::Syx, a++, 1, pt + "Time Trip Switch", "", 0, display[DisplayOffOn], 1, 0, 0, 1);
		p[i++] = new ParameterValueBits(MidiMessage::Syx, a++, 1, pt + "Time Trip Beat Keep", "", 0, display[DisplayOffOn], 1, 0, 0, 1);
	}

	for (unsigned j = 0; j < 2; ++j)
	{
		_snprintf(buf, 64, "Osc%d ", j+1);
		string pt = ps + buf;
		p[i++] = new ParameterValueBits(MidiMessage::Syx, a++, 1, pt + "Detune", "", 0, display[Display63n_63p], 7, 1+63, 1, 127);
		p[i++] = new ParameterValueBits(MidiMessage::Syx, a++, 1, pt + "Sub Oscillator Octave", "", 0, display[DisplaySubOscOctave], 2, 0, 0, 3);
		p[i++] = new ParameterValueBits(MidiMessage::Syx, a++, 1, pt + "Sub Oscillator Level", "", 0, NULL, 7, 0, 0, 127);
	}

	return p;
}

vector<ParameterValue *>
RolandVSynthXT::createPatchEnvelope(int zn, const char* s, uint32_t ofs)
{
	char buf[64];
	_snprintf(buf, 64, "%s Z[%d]: ", s, zn+1);
	string ps(buf);
	uint32_t a = ofs + zn * PatchEnvelope_Size;
	vector<ParameterValue *> p(13*10);
	unsigned i = 0;

	const char *enam[] = {
		"Osc1 Pitch", "Osc1 Tm/PW", "Osc1 Fr/Fat", "Osc1 Level",
		"Osc2 Pitch", "Osc2 Tm/PW", "Osc2 Fr/Fat", "Osc2 Level",
		"COSM1 Prm1", "COSM1 Prm2",
		"COSM2 Prm1", "COSM2 Prm2",
		"TVA Level"
	};

	for (unsigned j = 0; j < 13; ++j)
	{
		_snprintf(buf, 64, "Env%d (%s) ", j+1, enam[j]);
		string pt = ps + buf;

		p[i++] = new ParameterValueBits(MidiMessage::Syx, a, 2, pt + "Attack Time", "", 0, display[DisplayTimeNotes], 4, 0, 0, 144);
		a += 2;
		p[i++] = new ParameterValueBits(MidiMessage::Syx, a, 2, pt + "Decay Time", "", 0, display[DisplayTimeNotes], 4, 0, 0, 144);
		a += 2;
		p[i++] = new ParameterValueBits(MidiMessage::Syx, a++, 1, pt + "Sustain", "", 0, NULL, 7, 0, 0, 127);
		p[i++] = new ParameterValueBits(MidiMessage::Syx, a, 2, pt + "Release Time", "", 0, display[DisplayTimeNotes], 4, 0, 0, 144);
		a += 2;
		p[i++] = new ParameterValueBits(MidiMessage::Syx, a++, 1, pt + "Velocity Curve", "", 0, display[DisplayVelocityCurve], 3, 0, 0, 7);
		p[i++] = new ParameterValueBits(MidiMessage::Syx, a++, 1, pt + "Velocity Sensitivity", "", 0, display[Display63n_63p], 7, 1+63, 1, 127);
		p[i++] = new ParameterValueBits(MidiMessage::Syx, a++, 1, pt + "Attack Velocity Sensitivity", "", 0, display[Display63n_63p], 7, 1+63, 1, 127);
		p[i++] = new ParameterValueBits(MidiMessage::Syx, a++, 1, pt + "Decay Velocity Sensitivity", "", 0, display[Display63n_63p], 7, 1+63, 1, 127);
		a += (a & 0x80) ? 0x80 : 0; // Detect MIDI byte overflow
		p[i++] = new ParameterValueBits(MidiMessage::Syx, a++, 1, pt + "Release Velocity Sensitivity", "", 0, display[Display63n_63p], 7, 1+63, 1, 127);
		p[i++] = new ParameterValueBits(MidiMessage::Syx, a++, 1, pt + "Time Keyfollow", "", 0, display[Display200n_200p], 7, 44+20, 44, 84);
	}

	return p;
}

vector<ParameterValue *>
RolandVSynthXT::createPatchLFO(int zn, const char* s, uint32_t ofs)
{
	char buf[64];
	_snprintf(buf, 64, "%s Z[%d]: ", s, zn+1);
	string ps(buf);
	uint32_t a = ofs + zn * PatchLFO_Size;
	vector<ParameterValue *> p(5*7);
	unsigned i = 0;

	const char *lnam[] = { "Osc1", "Osc2" , "COSM1", "COSM2", "TVA" };

	for (unsigned j = 0; j < 5; ++j)
	{
		_snprintf(buf, 64, "LFO%d (%s) ", j+1, lnam[j]);
		string pt = ps + buf;

		p[i++] = new ParameterValueBits(MidiMessage::Syx, a++, 1, pt + "Waveform", "", 0, display[DisplayLFOWaveform], 3, 0, 0, 7);
		p[i++] = new ParameterValueBits(MidiMessage::Syx, a, 2, pt + "Rate", "", 0, display[DisplayTimeNotes], 4, 0, 0, 144);
		a += 2;
		p[i++] = new ParameterValueBits(MidiMessage::Syx, a++, 1, pt + "Offset", "", 0, display[DisplayLFOOffset], 3, 0, 0, 4);
		p[i++] = new ParameterValueBits(MidiMessage::Syx, a++, 1, pt + "Delay Time", "", 0, NULL, 7, 0, 0, 127);
		p[i++] = new ParameterValueBits(MidiMessage::Syx, a++, 1, pt + "Offset", "", 0, display[DisplayLFOFadeMode], 2, 0, 0, 3);
		p[i++] = new ParameterValueBits(MidiMessage::Syx, a++, 1, pt + "Fade Time", "", 0, NULL, 7, 0, 0, 127);
		p[i++] = new ParameterValueBits(MidiMessage::Syx, a++, 1, pt + "Key Sync", "", 0, display[DisplayOffOn], 1, 0, 0, 1);
	}

	return p;
}

vector<ParameterValue *>
RolandVSynthXT::createPatchCOSM(int zn, const char* s, uint32_t ofs)
{
	char buf[64];
	_snprintf(buf, 64, "%s Z[%d]: ", s, zn+1);
	string ps(buf);
	uint32_t a = ofs + zn * PatchCOSM_Size;
	vector<ParameterValue *> p(2+16);
	unsigned i = 0;

	p[i++] = new ParameterValueBits(MidiMessage::Syx, a++, 1, ps + "COSM Switch", "", 0, display[DisplayBypassOn], 1, 0, 0, 1);
	p[i++] = new ParameterValueBits(MidiMessage::Syx, a++, 1, ps + "COSM Type", "", 0, NULL, 5, 0, 0, 16);

	for (unsigned j = 0; j < 16; ++j)
	{
		_snprintf(buf, 64, "COSM Parameter %d", j+1);
		p[i++] = new ParameterValueBits(MidiMessage::Syx, a, 4, ps + buf, "", 0, display[Display20000n_20000p], 7, 12768+20000, 12768, 52768);
		a += 4;
	}

	return p;
}

vector<ParameterValue *>
RolandVSynthXT::createPatchArpeggio(int nn, const char* s, uint32_t ofs)
{
	char buf[64];
	_snprintf(buf, 64, "%s Z[%d]: ", s, nn+1);
	string ps(buf);
	uint32_t a = ofs + nn * PatchArpeggio_Size;
	vector<ParameterValue *> p(2+32);
	unsigned i = 0;

	p[i++] = new ParameterValueBits(MidiMessage::Syx, a++, 1, ps + "Control Switch", "", 0, display[DisplayNoteCtrl], 1, 0, 0, 1);
	p[i++] = new ParameterValueBits(MidiMessage::Syx, a++, 1, ps + "Control Number", "", 0, NULL, 7, 0, 0, 127);

	for (unsigned j = 0; j < 32; ++j)
	{
		_snprintf(buf, 64, "Step %d Velocity", j+1);
		p[i++] = new ParameterValueBits(MidiMessage::Syx, a, 2, ps + buf, "", 0, display[DisplayStepVelocity], 4, 0, 0, 128);
		a += 2;
	}

	return p;
}

