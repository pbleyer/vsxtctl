#ifndef ROLAND_FACTORY_H_
#define ROLAND_FACTORY_H_

#include "ParameterValue.h"

#include <stdint.h>
#include <vector>

using std::vector;

#include "MidiMessage.h"

#define ROLAND_VENDOR 0x41
#define ROLAND_DEFAULT_DEVICE_BYTES 1
#define ROLAND_DEFAULT_MODEL_BYTES 2
#define ROLAND_DEFAULT_ADDRESS_BYTES 4
#define ROLAND_DEFAULT_SIZE_BYTES 4

#define ROLAND_DEFAULT_DEVICE 0x10
#define ROLAND_DEFAULT_MODEL 0x0053
#define ROLAND_COMMAND_BYTES 1
#define ROLAND_COMMAND_RQ1 0x11
#define ROLAND_COMMAND_DT1 0x12
#define ROLAND_CHECKSUM_BYTES 1
// #define ROLAND_HEADER_BYTES (1 + ROLAND_DEVICE_BYTES + ROLAND_MODEL_BYTES + ROLAND_COMMAND_BYTES + ROLAND_ADDRESS_BYTES)

class RolandSysex
{
public:
	uint32_t device; ///< Device Id
	uint32_t model; ///< Model Id
	uint8_t command; ///< Request type
	uint32_t address; ///< Data address
	uint32_t size; ///< Request size
	vector<uint8_t> data; ///< Data value

	RolandSysex()
	: device(-1), model(-1), command(0), address(0), size(0), data()
	{ }
	RolandSysex(const RolandSysex &s)
	: device(s.device), model(s.model), command(s.command), address(s.address), size(s.size), data(s.data)
	{ }
	RolandSysex(int32_t d, int32_t m, uint8_t c, uint32_t a, uint32_t s)
	: device(d), model(m), command(c), address(a), size(s), data()
	{ }
	RolandSysex(int32_t d, int32_t m, uint8_t c, uint32_t a, uint32_t s, const uint8_t *b)
	: device(d), model(m), command(c), address(a), size(s), data((b) ? vector<uint8_t>(b, b + s) : vector<uint8_t>(s))
	{ }
	RolandSysex(uint8_t c, uint32_t a, uint32_t s)
	: device(-1), model(-1), command(c), address(a), size(s), data()
	{ }
	RolandSysex(uint8_t c, uint32_t a, uint32_t s, const uint8_t *b)
	: device(-1), model(-1), command(c), address(a), size(s), data((b) ? vector<uint8_t>(b, b + s) : vector<uint8_t>(s))
	{ }
	~RolandSysex(void) { }
	
	static uint8_t checksum(const RolandSysex &s);
	static uint8_t checksum(unsigned len, const uint8_t *bytes);
};

class RolandFactory
{
public:
	enum Value
	{
		RQ = ROLAND_COMMAND_RQ1,
		DT = ROLAND_COMMAND_DT1,
	};
	
public:
	uint8_t deviceBytes; ///< Device field size in bytes
	uint8_t modelBytes; ///< Model field size in bytes
	uint8_t addressBytes; ///< Address field size in bytes
	uint8_t sizeBytes; ///< Size field size in bytes
	uint32_t deviceDefault; ///< Default device
	uint32_t modelDefault; ///< Default model 

public:
	RolandFactory(const RolandFactory &rs)
	: deviceBytes(rs.deviceBytes), modelBytes(rs.modelBytes), addressBytes(rs.addressBytes), sizeBytes(rs.sizeBytes),
		deviceDefault(rs.deviceDefault), modelDefault(rs.modelDefault)
		{ }
	RolandFactory(uint8_t db = ROLAND_DEFAULT_DEVICE_BYTES, uint8_t mb = ROLAND_DEFAULT_MODEL_BYTES, 
		uint8_t ab = ROLAND_DEFAULT_ADDRESS_BYTES, uint8_t sb = ROLAND_DEFAULT_SIZE_BYTES,
		uint32_t dd = ROLAND_DEFAULT_DEVICE, uint32_t md = ROLAND_DEFAULT_MODEL)
	: deviceBytes(db), modelBytes(mb), addressBytes(ab), sizeBytes(sb),
		deviceDefault(dd), modelDefault(md)
	{ }
	virtual ~RolandFactory(void) { }
	
	int parse(RolandSysex *&s, unsigned len, const uint8_t *bytes) const;
	int format(const RolandSysex &syx, unsigned len, uint8_t *bytes) const;
	vector<uint8_t> format(const RolandSysex &syx) const;
	vector<uint8_t> format(uint8_t cmd, const ParameterValue &pv, uint32_t base = 0) const;

	MidiMessage formatMapGet(uint32_t adr, uint32_t siz, uint32_t base = 0);
	MidiMessage formatMapSet(uint32_t adr, uint32_t siz);


	// Format a message to get parameter from device
	vector<uint8_t> formatGet(const ParameterValue &p) const;
	// Format a message to set parameter in device
	vector<uint8_t> formatSet(const ParameterValue &p) const;
	// Parse message reply from device
	void parse(const vector<uint8_t> &d);

	inline unsigned headerBytes() const
	{
		return 1 + deviceBytes + modelBytes + ROLAND_COMMAND_BYTES + addressBytes;
	}

	inline unsigned length(const RolandSysex &s) const
	{ 
		return 1/*SOX*/ + headerBytes() + ((s.command == DT) ? s.size : sizeBytes) + ROLAND_CHECKSUM_BYTES + 1/*EOX*/;
	}
};

#endif // ROLAND_FACTORY_H_
