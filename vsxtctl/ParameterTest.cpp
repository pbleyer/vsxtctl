/** @file
	Parameter tests
*/

#include "RolandVSynthXT.h"
#include <fstream>

void 
testCSV()
{
	RolandVSynthXT vs;

	std::ofstream ofs;
	ofs.open("Parameter.csv");
	ofs << "Address,Bytes,Description,Unit,Display,Bits/byte,Minimum,Maximum,Step" << std::endl;
	vector<ParameterValue *>::const_iterator i = vs.parameter.begin();
	while (i != vs.parameter.end())
	{
		ofs << (*i)->toCSV() << std::endl;
		++i;
	}
	ofs.close();

	ofs.open("Display.csv");
	ofs << "Format,Name,Type,Minimum,Maximum,Step,Items" << std::endl;
	for (int j = 0; j < vs.DisplayEND; ++j)
		ofs << vs.display[j]->toCSV() << std::endl;
	ofs.close();
}
