// The following ifdef block is the standard way of creating macros which make exporting 
// from a DLL simpler. All files within this DLL are compiled with the VSXTCTL_EXPORTS
// symbol defined on the command line. This symbol should not be defined on any project
// that uses this DLL. This way any other project whose source files include this file see 
// VSXTCTL_API functions as being imported from a DLL, whereas this DLL sees symbols
// defined with this macro as being exported.
#ifdef VSXTCTL_EXPORTS
#define VSXTCTL_API __declspec(dllexport)
#else
#define VSXTCTL_API __declspec(dllimport)
#endif

// This class is exported from the vsxtctl.dll
class VSXTCTL_API Cvsxtctl {
public:
	Cvsxtctl(void);
	// TODO: add your methods here.
};

extern VSXTCTL_API int nvsxtctl;

VSXTCTL_API int fnvsxtctl(void);
