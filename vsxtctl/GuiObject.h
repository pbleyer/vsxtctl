/** @file
*/
#ifndef GUIOBJECT_H_
#define GUIOBJECT_H_

#include <Windows.h>

#include "resource.h"

#include "ParameterDisplay.h"

#define GUIKNOB_PIXELS 32
#define GUIKNOB_STEPS (992/32)
#define GUIKNOB_RANGE 200


class GuiObject
{
public:
	HWND window;

public:
	GuiObject(HWND wnd = NULL)
	: window(wnd) { }
	virtual ~GuiObject(void) { }

	virtual void update() { /* Sync the GUI with the value */ }
};

/** A GuiObject associated with a VST parameter */
class GuiParameter: public GuiObject
{
public:
	AudioEffect *effect; ///< Pointer to the VST effect
	VstInt32 index; ///< VST effect parameter index
	ParameterDisplay *display; ///< Parameter display information
	float defval; ///< Default value

public:
	GuiParameter(AudioEffect *eff = NULL, VstInt32 idx = 0, ParameterDisplay *dpy = NULL, float dv = 0.f);
	: GuiObject(), effect(eff), index(idx), display(dpy), defval(dv) { }
	~GuiParameter(void) { }

	virtual float getValue() const { return effect->getParameter(index); }
	virtual void setValue(float v) { effect->setParameter(index, v); update(); }
	virtual void resetValue() { effect->setParameter(index, defval); update(); }
};

/** Knob */
class GuiKnob: public GuiParameter
{
public:
	static unsigned _wcreg; ///< Window class registration/deregistration refcount
	HBITMAP bitmap; ///< Bitmap drawing

public:
	GuiKnob(AudioEffect *eff = NULL, VstInt32 idx = 0, ParameterDisplay *dpy = NULL, float dv = 0.f);
	virtual ~GuiKnob();

	static LRESULT WINAPI WndProc(HWND hWnd, UINT mesg, WPARAM wParam, LPARAM lParam);
};


#endif // GUIOBJECT_H_
