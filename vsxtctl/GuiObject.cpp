/** @file
	GuiObject
*/

#include "GuiObject.h"

#include <Windows.h>
#include <WindowsX.h>
#include <tchar.h>

static TCHAR szKnobClass[] = _T("GuiKnob Class");
static TCHAR szKnobTitle[] = _T("A VSTTst GUI Knob");

/* Knob */

int GuiKnob::_wcreg = 0;

GuiKnob::GuiKnob(AudioEffect *eff, VstInt32 idx, float dv, HBITMAP bmp, HWND parent, int x, int y)
: GuiParameter(eff, idx, dpy, dv), bitmap(bmp)
{
	if (!_wcreg) // Register class
	{
		WNDCLASSEX wc;
		wc.cbSize = sizeof(WNDCLASSEX);
		wc.style = CS_HREDRAW | CS_VREDRAW; // CS_OWNDC
		wc.lpfnWndProc = WndProc;
		wc.cbClsExtra = 0;
		wc.cbWndExtra = 0;
		wc.hInstance = (HINSTANCE)hInstance;
		wc.hIcon = LoadIcon((HINSTANCE)hInstance, MAKEINTRESOURCE(IDI_APPLICATION));
		wc.hCursor = LoadCursor(NULL, IDC_ARROW);
		wc.hbrBackground = (HBRUSH)(COLOR_WINDOW + 1); // (HBRUSH)GetStockObject(WHITE_BRUSH)
		wc.lpszMenuName = NULL;
		wc.lpszClassName = szKnobClass;
		wc.hIconSm = LoadIcon((HINSTANCE)hInstance, MAKEINTRESOURCE(IDI_APPLICATION));

		if (!RegisterClassEx(&wc)) ;
	}
	++_wcreg;

	// bitmap = LoadBitmap((HINSTANCE)hInstance, MAKEINTRESOURCE(IDB_KNOB));

	window = CreateWindowEx(
		0,
		szKnobClass, szKnobTitle,
		WS_CHILD | WS_VISIBLE, // WS_OVERLAPPEDWINDOW
		x, y, // CW_USEDEFAULT, CW_USEDEFAULT,
		32, 32,
		(HWND)parent,
		NULL, // Menu or child id
		(HINSTANCE)hInstance,
		NULL // Window data pointer
	);
	SetWindowLong(hwnd, GWL_USERDATA, (long)this); // Attach object instance to window
}

GuiKnob::~GuiKnob()
{
	DestroyWindow(hwnd);
	--wcreg;
	if (!wcreg)
		UnregisterClass(szKnobClass, (HINSTANCE)hInstance);
}

LRESULT WINAPI
GuiKnob::WndProc(HWND hWnd, UINT mesg, WPARAM wParam, LPARAM lParam)
{
	// static bool moving = false;
	static HWND prevw = NULL;
	static POINT cckpt;
	static float kval;
	BITMAP bm;
	Knob *knob = (Knob *)GetWindowLong(hWnd, GWL_USERDATA); // Get object pointer

	if (!knob)
		goto end;

	switch (mesg)
	{
	case WM_LBUTTONDOWN:
		cckpt.x = GET_X_LPARAM(lParam), cckpt.y = GET_Y_LPARAM(lParam);
		if (DragDetect(hWnd, cckpt))
		{
			// moving = true;
			// SetFocus(hWnd);
			prevw = SetCapture(hWnd); // Remember previous capture window
			kval = knob->getValue();
		}
		break;

	case WM_LBUTTONUP:
		SetCapture(prevw); // Restore previous capture window
		break;

	case WM_MOUSEMOVE:
		if (wParam & MK_LBUTTON)
		{
			int dy = cckpt.y- GET_Y_LPARAM(lParam);
			if (dy)
			{
				// float v = knob->getValue(), w = float(my)/KNOB_SCALE;
				float v = kval + float(dy)/KNOB_RANGE;
				v = (v < 0.f) ? 0.f : (v > 1.f) ? 1.f : v;
				knob->setValue(v);
				InvalidateRect(hWnd, NULL, FALSE);
			}
		}
		break;

	case WM_PAINT:
		{
			PAINTSTRUCT ps;
			int st = (int)(knob->getValue() * (KNOB_STEPS - 1));
			HDC hdc = BeginPaint(hWnd, &ps);

			HDC hmdc = CreateCompatibleDC(hdc);
			HBITMAP hbmo = (HBITMAP)SelectObject(hmdc, knob->getBitmap());
			GetObject(knob->getBitmap(), sizeof(BITMAP), &bm);
			BitBlt(hdc, 0, 0, bm.bmWidth, bm.bmWidth, hmdc, 0, st*bm.bmWidth, SRCCOPY);
			SelectObject(hdc, hbmo);
			DeleteDC(hmdc);

			EndPaint(hWnd, &ps);
			// knob->idle();
		}
		break;

	case WM_MOUSEWHEEL:
			{
				int ks = GET_KEYSTATE_WPARAM(wParam);
				int zd = GET_WHEEL_DELTA_WPARAM(wParam); // WHEEL_DELTA, which is 120
				float v = knob->getValue() + float(zd)*.0001f;
				v = (v < 0.f) ? 0.f : (v > 1.f) ? 1.f : v;
				knob->setValue(v);
				InvalidateRect(hWnd, NULL, FALSE);
			}
		break;

	case WM_KEYDOWN:
		{
			// k.character = wParam;
			// knob->onKeyDown(k);
			if (wParam == VK_UP || wParam == VK_DOWN)
			{
				float v = knob->getValue() + (wParam == VK_UP) ? 0.1f : -0.1f;
				v = (v < 0.f) ? 0.f : (v > 1.f) ? 1.f : v;
				knob->setValue(v);
				InvalidateRect(hWnd, NULL, FALSE);
			}
		}
		break;

	case WM_KEYUP:
		// k.character = wParam;
		// knob->onKeyUp(k);
		// InvalidateRect(hWnd, NULL, FALSE);
		break;

	case WM_DESTROY:
		PostQuitMessage(0);
		break;
	}

end:
	return DefWindowProc(hWnd, mesg, wParam, lParam);
}
