/** @file
*/
#ifndef UTILITY_H_
#define UTILITY_H_

#include <stdint.h>

/** Bundle value into MIDI 7-bit representation */
inline uint32_t
midiBundle(uint8_t nb, uint32_t v)
{
	uint32_t r = 0;
	for (int i = nb - 1; i >= 0; --i)
	{
		r <<= 8;
		r |= (v>>(7*i)) & 0x7f;
	}
	return r;
}

inline uint32_t
midiUnbundle(uint8_t nb, uint32_t v)
{
	uint32_t r = 0;
	for (int i = nb - 1; i >= 0; --i)
	{
		r <<= 7;
		r |= (v>>(8*i)) & 0x7f;
	}
	return r;
}

/** Add two midi bundled values */
inline uint32_t
midiAdd(uint8_t nb, uint32_t aa, uint32_t ab)
{
	return midiBundle(nb, midiUnbundle(nb, aa) + midiUnbundle(nb, ab));
}

/** Increment midi bundled value by amount */
inline uint32_t
midiIncrement(uint8_t nb, uint32_t aa, uint32_t ofs)
{
	return midiBundle(nb, midiUnbundle(nb, aa) + ofs);
}

#endif // UTILITY_H_
