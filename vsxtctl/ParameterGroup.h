#ifndef PARAMETER_GROUP_H_
#define PARAMETER_GROUP_H_

#include "ParameterValue.h"

class ParameterGroup
{
public:
	vector<ParameterValue *> parameter; ///< Parameter array

public:
	ParameterGroup(unsigned len = 0, ParameterValue *pv[] = NULL);
	ParameterGroup(const ParameterGroup &g): parameter(g.parameter) { }
	virtual ~ParameterGroup(void) { }
	
	/** Get parameter by address */
	ParameterValue *getParameterValue(uint32_t adr) const;
	/** Count number of regions (contiguous addresses) */
	unsigned regions() const;

	/** Get the total size in bytes */
	unsigned size() const;
};

#endif // PARAMETER_GROUP_H_
