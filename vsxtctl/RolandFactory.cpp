#include "RolandFactory.h"
#include "Utility.h"

int
RolandFactory::parse(Sysex *&syx, unsigned len, const uint8_t *b) const
{
	int r = 0;
	if (len < SYSEX_FRAME_BYTES + headerBytes() + ROLAND_CHECKSUM_BYTES)
		return -1; // Invalid length

	unsigned i = 0;
	if (b[i++] != SYSEX_SOX)
		return -2; // Invalid sysex start frame
	
	if (checksum(len - SYSEX_FRAME_BYTES - ROLAND_CHECKSUM_BYTES, &b[i]) != b[len - 2])
		return -3; // Invalid checksum

	if (b[i++] != ROLAND_VENDOR)
		return -4; // Invalid vendor

	Sysex *s = new Sysex(0, 0, 0, 0, 0);

	for (int j = 0; j < deviceBytes; ++j)
	{
		s->device <<= 8;
		s->device |= b[i++];
	}

	for (int j = 0; j < modelBytes; ++j)
	{
		s->model <<= 8;
		s->model |= b[i++];
	}

	s->command = b[i++];

	for (int j = 0; j < addressBytes; ++j)
	{
		s->address <<= 8;
		s->address |= b[i++];
	}

	if (s->command == RQ)
	{
		for (int j = 0; j < sizeBytes; ++j)
		{
			s->size <<= 8;
			s->size |= b[i++];
		}
		s->size = midiUnbundle(sizeBytes, s->size);
	}
	else if (s->command == DT)
	{
		if (len <= i)
		{
			r = -5; // Invalid data length
			goto error;
		}
		unsigned dl = len - i - 1;
		s->data = vector<uint8_t>(b, b + dl);
		i += dl;
	}

	++i; // Skip checksum
	if (b[i++] != SYSEX_EOX)
	{
		r = -6; // Invalid sysex end frame
		goto error;
	}
	
	if (i > len)
	{
		r = -7; // Internal error
		goto error;
	}

	syx = s;
	return i; // Bytes parsed

error:
	delete s;
	return r;
}

int 
RolandFactory::format(const Sysex& s, unsigned len, uint8_t *b) const
{
	if (len < length(s))
		return -1; // Invalid length

	unsigned i = 0;
	b[i++] = SYSEX_SOX;
	b[i++] = ROLAND_VENDOR;
	uint32_t t = (s.device != -1) ? s.device : deviceDefault;
	for (int j = deviceBytes - 1; j >= 0; --j)
		b[i++] = (uint8_t)((t>>(8*j)) & 0x7f);
	t = (s.model != -1) ? s.model : modelDefault;
	for (int j = modelBytes - 1; j >= 0; --j)
		b[i++] = (uint8_t)((t>>(8*j)) & 0x7f);
	b[i++] = s.command;
	for (int j = addressBytes - 1; j >= 0; --j)
		b[i++] = (uint8_t)((s.address>>(8*j)) & 0x7f);
	if (s.command == RQ)
	{
		uint32_t bs = midiBundle(sizeBytes, s.size);
		for (int j = sizeBytes - 1; j >= 0; --j)
			b[i++] = (uint8_t)(bs>>(8*j));
	}
	else if (s.command == DT)
	{
		for (unsigned j = 0; j < s.size; ++j)
			b[i++] = s.data[j];
	}
	b[i++] = checksum(s);
	b[i++] = SYSEX_EOX;
	return i;
}

vector<uint8_t> *
RolandFactory::format(uint8_t cmd, const ParameterValue &pv, uint32_t base) const
{
	Sysex s(cmd, pv.address + base, pv.bytes);
	if (cmd == DT)
	{
		s.data.resize(pv.bytes);
		pv.setQuery(pv.bytes, s.data.data());
	}
	return format(s);
}

vector<uint8_t> *
RolandFactory::format(const Sysex &s) const
{
	unsigned l = length(s);
	vector<uint8_t> *v = new vector<uint8_t>(l);
	format(s, v->size(), v->data());
	return v;
}

uint8_t 
RolandFactory::checksum(const Sysex &s) const
{
	uint8_t c = 0;
	c += ROLAND_VENDOR;
	uint32_t t = (s.device != -1) ? s.device : deviceDefault;
	for (int i = deviceBytes - 1; i >= 0 ; --i)
		c += (uint8_t)((t>>(8*i)) & 0x7f);
	t = (s.model != -1) ? s.model : modelDefault;
	for (int i = modelBytes - 1; i >= 0 ; --i)
		c += (uint8_t)((t>>(8*i)) & 0x7f);
	c += s.command;
	for (int i = addressBytes - 1; i >= 0 ; --i)
		c += (uint8_t)((s.address>>(8*i)) & 0x7f);
	if (s.command == RQ)
	{
		uint32_t bs = midiBundle(sizeBytes, s.size);
		for (int i = sizeBytes - 1; i >= 0; --i)
			c += (uint8_t)((bs>>(8*i)) & 0x7f);
	}
	else if (s.command == DT)
	{
		for (unsigned i = 0; i < s.size; ++i)
			c += s.data[i];
	}
	return (0x80 - c) & 0x7f;
}

uint8_t
RolandFactory::checksum(unsigned len, const uint8_t *b)
{
	uint8_t c = 0;
	for (unsigned i = 0; i < len; ++i)
		c += b[i];
	return (0x80 - c) & 0x7f;
}
