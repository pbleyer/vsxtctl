#include "ParameterGroup.h"
#include "Utility.h"

#include <algorithm>

ParameterGroup::ParameterGroup(unsigned len, ParameterValue *pv[])
: parameter(len)
{
	if (pv)
		for (unsigned i = 0; i < len; ++i)
			parameter[i] = pv[i];

	std::sort(parameter.begin(), parameter.end(), 
		[](const ParameterValue *a, const ParameterValue *b) { return a->address < b->address; }
	);
}

ParameterValue *
ParameterGroup::getParameterValue(uint32_t adr) const
{
	vector<ParameterValue *>::const_iterator i = parameter.begin();
	while (i != parameter.end())
	{
		if ((*i)->address == adr)
			return *i;
		++i;
	}
	return NULL;
}

unsigned
ParameterGroup::size() const
{
	vector<ParameterValue *>::const_iterator i = parameter.begin();
	unsigned bs = 0;
	while (i != parameter.end())
	{
		bs += (*i)->bytes;
		++i;
	}
	return bs;
}

unsigned
ParameterGroup::regions() const 
{
	if (parameter.size() == 0)
		return 0;

	unsigned r = 1;
	vector<ParameterValue *>::const_iterator i = parameter.begin();
	uint32_t a = (*i)->address;
	++i;

	while (i != parameter.end())
	{
		uint32_t b = (*i)->address;
		a = midiUnbundle(4, a);
		a += (*i)->bytes;
		a = midiBundle(4, a);
		if (a != b)
			++r;
		a = b;
		++i;
	}
	return r;
}
