// vsxtctl.cpp : Defines the exported functions for the DLL application.
//

//#include "stdafx.h"
#include "vsxtctl.h"


// This is an example of an exported variable
VSXTCTL_API int nvsxtctl=0;

// This is an example of an exported function.
VSXTCTL_API int fnvsxtctl(void)
{
    return 42;
}

// This is the constructor of a class that has been exported.
// see vsxtctl.h for the class definition
Cvsxtctl::Cvsxtctl()
{
    return;
}
