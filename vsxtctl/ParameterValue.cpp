#include "ParameterValue.h"

#include <algorithm>
#include <stdexcept>

string
ParameterValue::toCSV() const
{
	char buf[32];
	string s;
	_snprintf(buf, 32, "0x%08x,%d,", address, bytes);
	s += string(buf) + name + "," + label + ",";
	if (display)
		s += display->name;
	return s;
}

string
ParameterValueFloat::toCSV() const
{
	string s = ParameterValue::toCSV();
	char buf[32];
	_snprintf(buf, 32, ",%g", minimum);
	s += buf;
	_snprintf(buf, 32, ",%g", maximum);
	s += buf;
	_snprintf(buf, 32, ",%g", step);
	s += buf;
	return s;
}


unsigned
ParameterValueBits::formatMap(unsigned len, uint8_t *buf) const
{
	unsigned r = 0;
	switch (type)
	{
	case MidiMessage::Syx:
		{
			if (len < bytes)
				throw std::length_error("Buffer too short");
			bits_t v = (flags & Invert) ? invert(bytes, bits, value) : value;
			for (int i = 0; i < bytes; ++i)
				buf[i] = (v>>((bytes-1-i)*bits)) & ((1<<bits)-1); // MSB to LSB
			r = bytes;
		}
		break;

	case MidiMessage::Cc:
		if (len < 1)
			throw std::length_error("Buffer too short");
		buf[0] = value & 0x7f;
		r = 1;
		break;
	}
	return r;
}

unsigned 
ParameterValueBits::parseMap(unsigned len, const uint8_t *buf)
{
	unsigned r = 0;
	switch (type)
	{
	case MidiMessage::Syx:
		{
			if (len < bytes)
				throw std::length_error("Buffer too short");
			bits_t v = 0;
			for (unsigned i = 0; i < bytes; ++i)
			{
				v <<= bits;
				v |= buf[i] & ((1<<bits)-1); // MSB to LSB
			}
			value  = (flags & Invert) ? invert(bytes, bits, v) : v;
			r = bytes;
		}
		break;

	case MidiMessage::Cc:
		if (len < 1)
			throw std::length_error("Buffer too short");
		value = buf[0] & 0x7f;
		r = 1;
		break;
	}
	return r;
}

ParameterValueBits::bits_t 
ParameterValueBits::invert(uint8_t byt, uint8_t bit, bits_t v)
{
	bits_t r = 0;
	for (uint8_t i = 0; i < byt; ++i)
	{
		r <<= bit;
		r |= (v>>(bit*i)) & ((1<<bit)-1);
	}
	return r;
}

string
ParameterValueBits::toCSV() const
{
	string s = ParameterValue::toCSV();
	char buf[32];
	_snprintf(buf, 32, ",%d", bits);
	s += buf;
	_snprintf(buf, 32, ",%d", minimum);
	s += buf;
	_snprintf(buf, 32, ",%d", maximum);
	s += buf;
	_snprintf(buf, 32, ",%d", step);
	s += buf;
	return s;
}


/*
float
ParameterValueBytes::getParameter() const
{
	uint64_t l = 0;
	uint64_t t = 0;
	vector<uint8_t>::const_iterator i = value.begin();
	while (i != value.end())
	{
		l <<= bits;
		l |= *i & ((1<<bits)-1);
		t <<= bits;
		t |= ((1<<bits)-1);
		++i;
	}
	return float(l)/float(t);
}

void
ParameterValueBytes::setParameter(float f)
{
	// @todo
}
*/