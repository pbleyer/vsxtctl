// stdafx.h : include file for standard system include files,
// or project specific include files that are used frequently, but
// are changed infrequently
//

#pragma once

#include "targetver.h"

#define WIN32_LEAN_AND_MEAN             // Exclude rarely-used stuff from Windows headers
// Windows Header Files:
#include <windows.h>

#include <AEffect.h>
#include <AEffectX.h>
#include <AEffEditor.h>
#include <AudioEffect.h>
#include <AudioEffectX.h>
#include <VSTFXStore.h>

// TODO: reference additional headers your program requires here
