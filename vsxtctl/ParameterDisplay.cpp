#include "ParameterDisplay.h"

template<typename T> static string 
tagToCSV(const map<T,string> &m, const char *fmt)
{
	string s = "\"";
	map<T,string>::const_iterator i = m.begin();

	bool first = true;
	// for (auto &p : m)
	while (i != m.end())
	{
		char buf[32];
		_snprintf(buf, 32, fmt, i->first);
		s += (first) ? "" : " ";
		s += string(buf) + ":" + "\"\"" + i->second + "\"\"";
		first = false;
		++i;
	}

	s += "\"";
	return s;
}

string
ParameterDisplay::toCSV() const
{
	return name + "," + format + "," + "None";
}

string
ParameterDisplayFloat::toCSV() const
{
	string s = name + "," + format + "," + "Float";
	char buf[32];
	_snprintf(buf, 32, ",%g", minimum);
	s += buf;
	_snprintf(buf, 32, ",%g", maximum);
	s += buf;
	_snprintf(buf, 32, ",%g", step);
	s += buf;
	s += ",";
	if (tag.size())
		s += tagToCSV<float>(tag, "%g");
	return s;
}

string
ParameterDisplayInteger::toCSV() const
{
	string s = name + "," + format + "," + "Integer";
	char buf[32];
	_snprintf(buf, 32, ",%d", minimum);
	s += buf;
	_snprintf(buf, 32, ",%d", maximum);
	s += buf;
	_snprintf(buf, 32, ",%d", step);
	s += buf;
	s += ",";
	if (tag.size())
		s += tagToCSV<long>(tag, "%d");
	return s;
}

string
ParameterDisplayBigInteger::toCSV() const
{
	string s = name + "," + format + "," + "Integer";
	char buf[32];
	_snprintf(buf, 32, ",%lld", minimum);
	s += buf;
	_snprintf(buf, 32, ",%lld", maximum);
	s += buf;
	_snprintf(buf, 32, ",%lld", step);
	s += buf;
	s += ",";
	if (tag.size())
		s += tagToCSV<int64_t>(tag, "%lld");
	return s;
}
