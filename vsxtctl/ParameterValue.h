#ifndef PARAMETER_VALUE_H_
#define PARAMETER_VALUE_H_

#include "MidiMessage.h"
#include "ParameterDisplay.h"

#include <stdint.h>
#include <vector>
#include <utility>
#include <string>
#include <map>

using std::vector;
using std::string;
using std::pair;
using std::map;

class ParameterValue
{
public:
	enum Value
	{
		// Flags
		Invert = 1<<0, ///< MSB-LSB or LSB-MSB
		Patch = 1<<1, ///< Address is patch offset
		Always = 1<<2, ///< Parameter message needs to be sent/processed always
		// Initialized = 1<<2, ///< Value has been initialized
	};

public:
	uint8_t type; ///< Parameter type
	uint32_t address; ///< Address offset
	uint8_t bytes; ///< Number of register bytes
	string name; ///< Parameter name
	string label; ///< Parameter label
	uint8_t flags; ///< Bitflags
	ParameterDisplay *display; ///< Parameter display
	// VstParameterProperties properties; ///< Parameter properties
	float raw; ///< Raw value
	int64_t last; ///< Last update timestamp

public:
	ParameterValue(uint8_t typ = MidiMessage::None, uint32_t adr = 0, uint8_t bs = 1, const string &nam = "", const string &lbl = "",
		uint8_t flg = 0, ParameterDisplay *dpy = NULL)
		: type(typ), address(adr), bytes(bs), name(nam), label(lbl), flags(flg), display(dpy), raw(-1.f), last(0) { }
	virtual ~ParameterValue(void) { }

	virtual float getParameter() const { return 0.f; }
	virtual void setParameter(float f) { raw = f; }
	virtual void getParameterDisplay(unsigned l, char *s, float v) const
	{
		if (display) display->display(l, s, v);
		else _snprintf(s, l, "%f", v);
	}
	virtual void getParameterDisplay(unsigned l, char *s) const
	{
		return getParameterDisplay(l, s, getParameter());
	}

	/** Check if value will change parameter */
	virtual bool testParameter(float f) const { return f != raw; }
	/** Get parameter raw value */
	virtual float getRaw() { return raw; }
	/** Check if parameter has not been initialized by analyzing the raw value */
	virtual bool isReset() { return raw < 0.f; }
	/** Reset the parameter to the uninitialized state */
	virtual void reset() { raw = -1.f; }

	/** Get parameter mapped representation value */
	virtual unsigned formatMap(unsigned len, uint8_t *d) const;
	/** Set parameter value from mapped representation */
	virtual unsigned parseMap(unsigned len, const uint8_t *d);

	virtual unsigned formatMap(vector<uint8_t> &v) { return formatMap(v.size(), v.data());
	virtual unsigned parseMap(const vector<uint8_t> &v) { return parseMap(v.size(), v.data());

	virtual string toCSV() const;
};

class ParameterValueFloat: public ParameterValue
{
public:
	float value;
	float minimum;
	float maximum;
	float step;

public:
	ParameterValueFloat(uint8_t typ = MidiMessage::None, uint32_t adr = 0, uint8_t bs = 1, const string &nam = "", const string &lbl = "",
		uint8_t flg = 0, ParameterDisplay *dpy = NULL,
		float val = 0.f, float min = 0.f, float max = 1.f, float stp = 0.f)
	: ParameterValue(typ, adr, bs, nam, lbl, flg, dpy),
		value((val < min) ? min : (val > max) ? max : val), minimum(min), maximum(max), step(stp) { }

	float getParameter() const { return (step == 0.f) ? value : long(value/step)*step; }
	float translateParameter(float f) const { return (step == 0.f) ? f : long(f/step)*step; }
	void setParameter(float f) { ParameterValue::setParameter(f); value = translateParameter(f); }
	bool testParameter(float f) const { return translateParameter(f) != value; }

	unsigned formatValue(unsigned len, uint8_t *d) const;
	unsigned parseValue(unsigned len, const uint8_t *d);

	string toCSV() const;
};

class ParameterValueBits: public ParameterValue
{
public:
	typedef uint32_t bits_t;

public:
	uint8_t bits; ///< Bits packed in each byte
	bits_t value;
	bits_t minimum;
	bits_t maximum;
	bits_t step;

public:
	ParameterValueBits(uint8_t typ = MidiMessage::None, uint32_t adr = 0, uint8_t bs = 1, const string &nam = "", const string &lbl = "",
		uint8_t flg = 0, ParameterDisplay *dpy = NULL,
		uint8_t bit = 7, bits_t val = 0, bits_t min = 0, bits_t max = 0x7f, bits_t stp = 1)
		: ParameterValue(typ, adr, bs, nam, lbl, flg, dpy), value((val < min) ? min : (val > max) ? max : val),
		bits(bit), minimum(min), maximum(max), step(stp) { }

	float getParameter() const { return float(double(((step > 1) ? (value/step) * step : value) - minimum)/double(maximum - minimum));  }
	bits_t translateParameter(float f) const { double t = double(maximum - minimum)*double(f) + double(minimum) + .5f; return (step > 1) ? bits_t(t/step) * step : bits_t(t); }
	void setParameter(float f) { ParameterValue::setParameter(f); value = translateParameter(f); }
	virtual void getParameterDisplay(unsigned l, char *s, float v) const
	{
		if (display) display->display(l, s, v);
		else _snprintf(s, l, "%d", translateParameter(v));
	}
	bool testParameter(float f) const { return translateParameter(f) != value; }

	unsigned formatMap(unsigned len, uint8_t *d) const;
	unsigned parseMap(unsigned len, const uint8_t *d);

	string toCSV() const;

	static bits_t invert(uint8_t byt, uint8_t bit, bits_t v);
};

/*
class ParameterValueBitsMulti: public ParameterValue
{
public:
	typedef uint32_t bits_t;
	struct Region 
	{
		bits_t minimum;
		bits_t maximum;
		bits_t step;
	};

public:
	uint8_t bits; ///< Bits packed in each byte
	bits_t value;
	vector<Region> region;
	vector<unsigned> size; ///< Region size
	unsigned total; ///< Total size

public:
	ParameterValueBitsMulti(uint32_t adr = 0, uint8_t bs = 1, const string &nam = "", const string &lbl = "",
		uint8_t flg = 0, ParameterDisplay *dpy = NULL,
		uint8_t bit = 7, bits_t val = 0, const vector<Region>& v = vector<Region>())
		: ParameterValue(adr, bs, nam, lbl, flg, dpy), 
		bits(bit), value(val), region(v) { initialize(); }

	void initialize()
	{
		total = 0;
		size = vector<unsigned>(region.size());
		for (unsigned i = 0; i < size.size(); ++i)
		{
			const Region &r = region[i];
			size[i] = ((r.step) ? (r.maximum - r.minimum) / r.step : (r.maximum - r.minimum)) + 1;
			total += size[i];
		}
	}

	unsigned getRegion(float f)
	{

	}

	float getParameter() const { 
		return float(((step > 1) ? (value/step) * step : value) - minimum)/float(maximum - minimum);  
	}


	bits_t translateParameter(float f) const { float t = float(maximum - minimum)*f + float(minimum) + .5f; return (step > 1) ? bits_t(t/step) * step : bits_t(t); }
	void setParameter(float f) { ParameterValue::setParameter(f); value = translateParameter(f); }
	virtual void getParameterDisplay(unsigned l, char *s, float v) const
	{
		if (display) display->display(l, s, v);
		else _snprintf(s, l, "%d", translateParameter(v));
	}

};

class ParameterValueString: public ParameterValue
{
public:
	uint8_t size; ///< Fixed string size
	string value; ///< String value
	uint8_t minimum; ///< Minimum value of each character
	uint8_t maximum; ///< Maximum value of each character
};
*/
/*
class ParameterValueInteger: public ParameterValue
{
public:
	long value;
	long minimum;
	long maximum;
	long step;
	const map<long,string> *description; ///< Parameter description

public:
	ParameterValueInteger(uint32_t adr = 0, uint8_t spn = 0, const char* nam = "",
		long val = 0, long min = 0, long max = 0x7f, long stp = 1,
		const map<long,string> *dm = NULL)
		: ParameterValue(adr, spn, nam), value(val), minimum(min), maximum(max), step(stp), description(dm) { }

	float getParameter() const { return float(((step > 1) ? (value/step) * step : value) - minimum)/float(maximum - minimum); }
	void setParameter(float f) { value = long(float(maximum - minimum)*f + float(minimum)); if (step > 1) value = (value/step) * step; }

	const char* toString() const;
	int fromString(const char *vd);

	vector<uint8_t> query();
	int reply(const vector<uint8_t> &v);
};
*/

/*
class ParameterValueBytes: public ParameterValue
{
public:
	vector<uint8_t> value;
	uint8_t bits; ///< Bits used from each byte
	uint8_t minimum;
	uint8_t maximum;
	uint8_t step;
	// map<uint32_t,string> description; ///< Parameter description

public:
	ParameterValueBytes(uint32_t adr = 0, uint8_t spn = 0, const char* nam = "",
		vector<uint8_t> val = vector<uint8_t>(), uint8_t bit = 4, uint8_t stp = 1, uint8_t min = 0, uint8_t max = 0x0f)
		: ParameterValue(adr, spn, nam), value(val), bits(bit), minimum(min), maximum(max), step(stp) { }

	float getParameter() const;
	void setParameter(float f);

	const char* toString() const;
	int fromString(const char *vd);

	vector<uint8_t> query();
	int reply(const vector<uint8_t> &v);
};
*/
#endif // PARAMETER_VALUE_H_
