#ifndef PARAMETER_DISPLAY_H_
#define PARAMETER_DISPLAY_H_

#include <stdio.h>
#include <string>
#include <map>
#include <stdint.h>

using std::string;
using std::map;

class ParameterDisplay
{
public:
	string format;
	string name;

public:
	ParameterDisplay(const ParameterDisplay &d): name(d.name), format(d.format) { }
	ParameterDisplay(const char *fmt = "%g"): name(""), format(fmt) { }
	ParameterDisplay(const char *nam, const char *fmt): name(nam), format(fmt)  { }
	virtual ~ParameterDisplay(void) { }

	virtual void display(unsigned l, char *s, float f) const { _snprintf(s, l, format.c_str(), f); s[l-1] = '\0'; }

	/** Get the total number of values or zero if the parameter is continuous */
	virtual unsigned levels() const { return 0; }

	virtual string toCSV() const;
};

class ParameterDisplayFloat: public ParameterDisplay
{
public:
	float minimum;
	float maximum;
	float step;
	map<float,string> tag;

public:
	ParameterDisplayFloat(const ParameterDisplayFloat &d)
	: ParameterDisplay(d), minimum(d.minimum), maximum(d.maximum), step(d.step), tag(d.tag) { }

	ParameterDisplayFloat(float mn = 0, float mx = 0x7f, float st = 1, 
		const char *fmt = "%.1f", const map<float,string> &t = map<float,string>())
	: ParameterDisplay(fmt), minimum(mn), maximum(mx), step(st), tag(t) { }

	ParameterDisplayFloat(const char *nam, float mn = 0, float mx = 0x7f, float st = 1, 
		const char *fmt = "%.1f", const map<float,string> &t = map<float,string>())
	: ParameterDisplay(nam, fmt), minimum(mn), maximum(mx), step(st), tag(t) { }
	
	~ParameterDisplayFloat(void) { }

	void display(unsigned l, char *s, float f) const
	{
		float g = (maximum - minimum)*f + minimum;
		float h = (step > 0.f) ? long(g/step)*step : g;
		map<float,string>::const_iterator i;
		if (!tag.size() || (i = tag.find(h)) == tag.end())
			_snprintf(s, l, format.c_str(), h);
		else
		{
			_snprintf(s, l, "%s", i->second.c_str());
		}
	}

	unsigned levels() const
	{
		return (step == 0.f) ? 0 : unsigned((maximum - minimum)/step) + 1;
	}

	virtual string toCSV() const;
};

class ParameterDisplayInteger: public ParameterDisplay
{
public:
	long minimum;
	long maximum;
	long step;
	map<long,string> tag;

public:
	ParameterDisplayInteger(const ParameterDisplayInteger &d)
	: ParameterDisplay(d), minimum(d.minimum), maximum(d.maximum), step(d.step), tag(d.tag) { }

	ParameterDisplayInteger(long mn = 0, long mx = 0x7f, long st = 1, const char *fmt = "%+d",
		const map<long,string> &t = map<long,string>())
	: ParameterDisplay(fmt), minimum(mn), maximum(mx), step(st), tag(t) { }

	ParameterDisplayInteger(const char *nam, long mn = 0, long mx = 0x7f, long st = 1, const char *fmt = "%+d",
		const map<long,string> &t = map<long,string>())
	: ParameterDisplay(nam, fmt), minimum(mn), maximum(mx), step(st), tag(t) { }

	~ParameterDisplayInteger(void) { }

	void display(unsigned l, char *s, float f) const
	{
		float g = float(maximum - minimum)*f + float(minimum);
		g += (g >= 0.f) ? .5f : -.5f;
		long h = (step > 1) ? long(g/step)*step : long(g);
		map<long,string>::const_iterator i;
		if (!tag.size() || (i = tag.find(h)) == tag.end())
			_snprintf(s, l, format.c_str(), h);
		else
			_snprintf(s, l, "%s", i->second.c_str());
	}

	unsigned levels() const
	{
		return (maximum - minimum)/((step == 0) ? 1 : step) + 1;
	}

	virtual string toCSV() const;
};

class ParameterDisplayBigInteger: public ParameterDisplay
{
public:
	int64_t minimum;
	int64_t maximum;
	int64_t step;
	map<int64_t,string> tag;

public:
	ParameterDisplayBigInteger(const ParameterDisplayBigInteger &d)
	: ParameterDisplay(d), minimum(d.minimum), maximum(d.maximum), step(d.step), tag(d.tag) { }

	ParameterDisplayBigInteger(int64_t mn = 0, int64_t mx = 0xffffffff, int64_t st = 1, const char *fmt = "%+lld",
		const map<int64_t,string> &t = map<int64_t,string>())
	: ParameterDisplay(fmt), minimum(mn), maximum(mx), step(st), tag(t) { }

	ParameterDisplayBigInteger(const char *nam, int64_t mn = 0, int64_t mx = 0xffffffff, int64_t st = 1, const char *fmt = "%+lld",
		const map<int64_t,string> &t = map<int64_t,string>())
	: ParameterDisplay(nam, fmt), minimum(mn), maximum(mx), step(st), tag(t) { }

	~ParameterDisplayBigInteger(void) { }

	void display(unsigned l, char *s, float f) const
	{
		double g = double(maximum - minimum)*f + double(minimum);
		g += (g >= 0.f) ? .5f : -.5f;
		int64_t h = (step > 1) ? int64_t(g/step)*step : int64_t(g);
		map<int64_t,string>::const_iterator i;
		if (!tag.size() || (i = tag.find(h)) == tag.end())
			_snprintf(s, l, format.c_str(), h);
		else
			_snprintf(s, l, "%s", i->second.c_str());
	}

	unsigned levels() const
	{
		return unsigned((maximum - minimum)/((step == 0) ? 1 : step) + 1);
	}

	virtual string toCSV() const;
};


#endif // PARAMETER_DISPLAY_H_
