/** @file
*/
#ifndef VSXTCTL_H_
#define VSXTCTL_H_

// The following ifdef block is the standard way of creating macros which make exporting 
// from a DLL simpler. All files within this DLL are compiled with the VSXTCTL_EXPORTS
// symbol defined on the command line. This symbol should not be defined on any project
// that uses this DLL. This way any other project whose source files include this file see 
// VSXTCTL_API functions as being imported from a DLL, whereas this DLL sees symbols
// defined with this macro as being exported.
#ifdef VSXTCTL_EXPORTS
#define VSXTCTL_API __declspec(dllexport)
#else
#define VSXTCTL_API __declspec(dllimport)
#endif

#include <Windows.h>
#include <AEffect.h>
#include <AEffectX.h>
#include <AEffEditor.h>
#include <AudioEffect.h>
#include <AudioEffectX.h>
#include <VSTFXStore.h>

#include "resource.h"

#include "RolandFactory.h"
#include "ParameterDevice.h"
#include "CircularBuffer.h"
#include "RolandVSynthXT.h"

class Knob
{
protected:
	static int wcreg; // Flag class registration
	HWND hwnd;
	HBITMAP bitmap;
	AudioEffect *effect;
	VstInt32 index; // parameter index
	float defval; // default value

public:
	Knob(HWND parent, int x, int y, AudioEffect *eff, VstInt32 idx, float dv = 0);
	virtual ~Knob();

	float getValue() { return effect->getParameter(index); }
	void setValue(float v) { effect->setParameter(index, v); }
	void resetValue() { effect->setParameter(index, defval); }
	HBITMAP getBitmap() { return bitmap; }
	
	static LRESULT WINAPI WndProc(HWND hWnd, UINT mesg, WPARAM wParam, LPARAM lParam);
};

class VSXTCtlGui: public AEffEditor
{
protected:
	HWND editorWindow; // Pointer to the editor top window
	ERect rect; // Holds the window size info
	HBITMAP image; // A bitmap

	Knob *knob0;

public:
	VSXTCtlGui(AudioEffect *eff = 0);
	virtual ~VSXTCtlGui();

	virtual bool getRect(ERect **r);
	virtual bool open(void *ptr);
	virtual void close();
	virtual void idle();

	static LRESULT WINAPI WndProc(HWND hWnd, UINT mesg, WPARAM wParam, LPARAM lParam);

#if VST_2_1_EXTENSIONS
	virtual bool onKeyDown(VstKeyCode &keyCode);	
	virtual bool onKeyUp(VstKeyCode &keyCode);		
	virtual bool onWheel(float distance);			
	virtual bool setKnobMode(VstInt32 val);
#endif
};

class VSXTCtl: public AudioEffectX
{
protected:
	RolandFactory factory;
	RolandVSynthXT device;
	CircularBuffer<vector<uint8_t>*, 256> outBuffer;
	char programName[kVstMaxProgNameLen + 1];

public:
	VSXTCtl(audioMasterCallback);
	virtual ~VSXTCtl();
	
	// Effect
	virtual bool getEffectName(char *name);
	virtual bool getVendorString(char *text);
	virtual bool getProductString(char *text);
	virtual VstInt32 getVendorVersion();
	virtual VstInt32 canDo(char *text);
	virtual VstIntPtr vendorSpecific (VstInt32 lArg, VstIntPtr lArg2, void* ptrArg, float floatArg);
	
	// Program
	virtual void setProgramName(char *name);
	virtual void getProgramName(char *name);

	// Parameters
	virtual void setParameter(VstInt32 index, float value);
	virtual float getParameter(VstInt32 index);
	virtual void getParameterLabel(VstInt32 index, char *label);
	virtual void getParameterDisplay(VstInt32 index, char *text);
	virtual void getParameterName(VstInt32 index, char *text);

	// Processing
	virtual void processReplacing(float **inputs, float **outputs, VstInt32 samples);
	virtual VstInt32 processEvents(VstEvents* events); // Get MIDI events
};

int VSXTCtlGuiInitialize(HINSTANCE hInst);
int VSXTCtlGuiDestroy(HINSTANCE hInst);
HWND VSXTCtlGuiCreateWindow(HINSTANCE hInst, HWND hWnd, ERect *r);
int VSXTCtlGuiDestroyWindow(HWND hWnd);

#endif // VSXTCTL_H_


// This class is exported from the VSXTCtl.dll
/* class VSXTCTL_API CVSXTCtl {
public:
	CVSXTCtl(void);
	// TODO: add your methods here.
};

extern VSXTCTL_API int nVSXTCtl;

VSXTCTL_API int fnVSXTCtl(void);
*/
